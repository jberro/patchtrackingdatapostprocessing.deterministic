% File describing the data to load, the figures to plot, setting
% temporal super-resolution realignment parameters, etc. Has to be used as
% an input of the loadAndPlotData_main function, e.g.:
% loadAndPlotData_main 'JSON files\experimentsFim1InPil1D_160308.m'
%
% PatchTrackingDataPostProcessing toolbox
% Julien Berro
% 2012-2016
% Yale University


%% Aliases
% 'aliases' contains the shortcuts for values used in the rest of the file
% (e.g. if 'colorActin' is an alias for [0, 0, 0], every time 'colorActin' is used as a value of a property, the property will be changed into [0,0,0]),
% Any alias has to be put between curly brackets (eg {'black',[0, 0, 0]})
aliases={...
    {'black',[0, 0, 0]},...
    {'lightBlack',[0.8, 0.8, 0.8]},...
    {'green',[0, 0.5, 0]},...
    {'lightGreen',[0.7059, 0.9333, 0.7059]},...
    {'blue',[0,0,1]},...
    {'lightBlue',[0.75,0.75,1]},...
    {'red',[1,0,0]},...
    {'lightRed',[1,0.75,0.75]},...
    {'symbolWT','none'},...
    {'dots','.'},...
    {'commentAliasErrorBars','confidenceInterval95pc'},...
    {'aliasErrorBars','none'},...
    {'calibration1', [3e5/800]},...
    {'calibration3', [6.7e5/(800*800/600)]},...
    {'calibration2', 1}...
    };

%% Default values
% 'defaultValues' contains value of parameters that should be used as default (and therefore can be omitted when defining other variables)
% The name of the fields of the 'defaultValues' has to start with 'default'
% and be followed by the name of the parameters whose default has to be set.
% For example, to set the default value for the variable delayFramesInSec to 0.93, add:
% defaultValues.defaultDelayFramesInSec=0.93;
% If a default value is defined for a variable, the variable can be omitted when defining the data or plots,
% Note that aliases can be used as defaul value (e.g. defaultValues.defaultCalibrationAUperMoleculePerSecond='calibration1')
defaultValues={};
defaultValues.defaultDelayFramesInSec=0.93; 
defaultValues.defaultExposureTimeInMs=50;
defaultValues.defaultCalibrationAUperMoleculePerSecond='calibration3';
defaultValues.defaultTimePeak=0;



%% Strains
% This structure stores information about the data for the strains to plot (e.g. path of the files to load, strain name, expoesure time, etc)
% All properties (except 'relativeSymbolSize') should be defined except if a default value as already been assigned in the defaultValues structure.
% Note that in general, if parameters values are strings of characters (except the parameter name starts with 'textToPrint' or for paths), they needs to be in allowed formats of Matlab variables (cannot contain spaces, or special caracter except undescore, has to start with a letter)
% 'name': unique id of the strain. It is used in the definition of the plots (e.g. fim1GFP_in_WT). It should be unique (no two strains can have the same name)
% 'mutant': identifier of the mutant (strains with the same genetic background should have the same mutant identifier)
% 'textToPrintMutant': text to print on graphs, etc
% 'fluorescentProtein': identifier of the fluorescent protein (strains with the same fluorescent protein should have the same fluorescentProtein identifier)
% 'textToPrintFluorescentProtein': text to print on graphs, etc
% 'exposureTimeInMs': exposure time used when aquiring the data (in milliseconds)
% 'delayFramesInSec': delay between 2 consecutive time points when acquiring the data (in seconds)
% 'calibrationAUperMoleculePerSecond': microscope calibration, i.e. number of arbitrary units of fluorescence one fluorescent protein emits in one second
% 'timePeak': absolute time (in seconds) at which the number of molecule peaks (typically 0 for actin in wild-type)
% 'color': color to use when plotting the average (should be an array of RGB values, see Matlab help for more info)
% 'colorErrorBars': color to use when plotting the error bars or error lines (an array of RGB values, see Matlab help for more info)
% 'symbol': symbol to use for the plots (should be a valid Matlab 'Marker')
% 'relativeSymbolSize' (optional): a multiplier for the size of the symbol used relatively to other symbols (can be omitted, default is 1)
% 'paths': array of paths of ConcentrationVsDistance.xls data files  (needs to be a valid Matlab paths)

strains={}; % initialisation of the strain list (Do not modify)

% First strain
newStrain=struct;% initialisation of the first strain (Do not modify)
newStrain.name='fim1GFP_WT'; % 'name', unique id of the strain.
newStrain.mutant='WT'; % identifier of the mutant
newStrain.textToPrintMutant='WT';% text to print on graphs, etc
newStrain.fluorescentProtein='Fim1GFP'; % identifier of the fluorescent protein (strains with the same fluorescent protein should have the same fluorescentProtein identifier)
newStrain.textToPrintFluorescentProtein='Fim1GFP';% text to print on graphs, etc
newStrain.color= 'black'; % color to use when plotting the average (should be an array of RGB values, see Matlab help for more info)
newStrain.colorErrorBars= 'lightBlack'; % color to use when plotting the error bar/line
newStrain.symbol= 'symbolWT';% symbol to use for the plots (should be a valid Matlab 'Marker')
newStrain.paths={... % array of paths of ConcentrationVsDistance.xls data files
    'C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\2015-12-15 Sp57\ConcentrationVsDistance.xls',...
    'C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\2015-12-15 Sp57 001\ConcentrationVsDistance.xls',...
    'C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\2015-12-15 Sp57 002\ConcentrationVsDistance.xls',...
    'C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\2015-12-15 Sp57 003\ConcentrationVsDistance.xls',...
    'C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\2015-12-15 Sp57 004\ConcentrationVsDistance.xls'...
    };
strains=[strains newStrain]; % addition of the current strain to the strain list

% Second strain
newStrain=struct;
newStrain.name='fim1GFP_pil1D';
newStrain.mutant='pil1D';
newStrain.textToPrintMutant='pil1D';
newStrain.fluorescentProtein='Fim1GFP';
newStrain.textToPrintFluorescentProtein='Fim1GFP';
newStrain.color= 'blue';
newStrain.colorErrorBars= 'lightBlue';
newStrain.symbol= 'none';
newStrain.paths={...
    'C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\2015-12-15 Sp234\ConcentrationVsDistance.xls',...
    'C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\2015-12-15 Sp234 001\ConcentrationVsDistance.xls',...
    'C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\2015-12-15 Sp234 002\ConcentrationVsDistance.xls',...
    'C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\2015-12-15 Sp234 003\ConcentrationVsDistance.xls',...
    'C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\2015-12-15 Sp234 004\ConcentrationVsDistance.xls',...
    'C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\2015-12-15 Sp234 005\ConcentrationVsDistance.xls',...
    'C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\2015-12-15 Sp234 006\ConcentrationVsDistance.xls'...
    };
strains=[strains newStrain];

%%% Template for another strain
% newStrain=struct;% initialisation of the first strain (Do not modify)
% newStrain.name=''; % 'name', unique id of the strain.
% newStrain.mutant=''; % identifier of the mutant
% newStrain.textToPrintMutant='';% text to print on graphs, etc
% newStrain.fluorescentProtein=''; % identifier of the fluorescent protein (strains with the same fluorescent protein should have the same fluorescentProtein identifier)
% newStrain.textToPrintFluorescentProtein='';% text to print on graphs, etc
% newStrain.color= ''; % color to use when plotting the average (should be an array of RGB values, see Matlab help for more info)
% newStrain.colorErrorBars= ''; % color to use when plotting the error bar/line
% newStrain.symbol= '';% symbol to use for the plots (should be a valid Matlab 'Marker')
% newStrain.paths={... % array of paths of ConcentrationVsDistance.xls data files
%     '',...
%     '',...
%     '',...
%     };
% strains=[strains newStrain]; % addition of the current strain to the strain list


%% Plots
% 'plots' contains all the group of figures to plot (each group of plot is independet, e.g. plot all the actin data first, then all the fimbrin data)
% 'name': unique id of the group of plots.
% 'strainsToPlot': strain identifier of the strains to plot (e.g. {'fim1GFP_in_WT','fim1GFP_pil1D'}). Note these strains need to defined in 'strains'
% 'figures': list of figures to plot. Each element of the list is a pair of data identifiers to plot versus each other (e.g. {'time','nbOfMolecules'})
%           Possible data identifiers are: time, nbOfMolecules, distance, speed, assemblyRate, MSD, pathLength
% 'errorBars': type of error bars/lines to plot. Possible values are:
%       regular (one standard deviation represented as error bars above and below the average),
%       line (one standard deviation represented as a line above and below the average),
%   	markers (one standard deviation represented as markers above and below the average),
%       line_and_markers (one standard deviation, with line and markers)
%       standard_error (one standard error of the mean)
%       confidenceInterval95pc (confidence interval at 95 percent represented as a line above and below the average),
%       none (no error bar or line shown)
% 'individualTracksToPlot': indices of the individual datasets (from individual patches) to plot. Indices can be an array of integers or 'all'. None will be plotted if omitted.
% 'limits' (optional): limits on the x (or y) axes for each variable. 'limits' are given as an array containing the upper and lower boundaries (e.g. [-12,12])
% 'ticks' (optional): ticks on the x (or y) axes for each variable. 'ticks' are given as an array of positions for the ticks(e.g. [0:0.1:0.9])

plots={}; % Initialisation of the plot list

% 1st group of plots
newPlot=struct; % Initialisation of the first group of plots
newPlot.name='allFimbrinPlots'; % id of the group of plots
newPlot.strainsToPlot={'fim1GFP_WT','fim1GFP_pil1D'};% identifiers of the strains to plot (e.g. {'fim1GFP_in_WT','fim1GFP_pil1D'}). Strains need to defined in 'strains'
newPlot.figures={... % list of figures to plot. Possible data identifiers are: time, nbOfMolecules, distance, speed, assemblyRate, MSD, pathLength
    {'time','nbOfMolecules'},... % pair of data identifiers to plot versus each other (e.g. {'time','nbOfMolecules'})
    {'time','speed'},...
    {'speed','nbOfMolecules'},...
    };
newPlot.errorBars='standard_error'; % type of error bars/lines to plot. Possible values: regular, line, markers, line_and_markers, standard_error, confidenceInterval95pc , none
newPlot.limits.time=[-20,20]; % limits for the time axis
newPlot.limits.nbOfMolecules=[0,1000];
newPlot.limits.speed=[0,0.4];
newPlot.limits.distance=[0,0.9];
newPlot.limits.assemblyRate=[-400,400];
newPlot.ticks.time=-10:2:10; % ticks on time axis
newPlot.ticks.nbOfMolecules=0:200:1000;
newPlot.ticks.speed=0:0.05:0.4;
newPlot.ticks.distance=0:0.1:0.9;
newPlot.ticks.assemblyRate=[-1:0.5:1]*400;
plots=[plots newPlot]; % Addition of the group of plots to the list (Do not delete)

% 2nd group of plot
newPlot=struct;
newPlot.name='allTracksWTFimbrin';
newPlot.strainsToPlot={'fim1GFP_WT'}; % e.g. {'fim1GFP_WT','fim1GFP_pil1D'}
newPlot.figures={...
    {'time','nbOfMolecules'},... % e.g. {'time','nbOfMolecules'} or {'speed','nbOfMolecules'}
    {'time','speed'},...
    };
newPlot.individualTracksToPlot={...
    {'fim1GFP_WT','all'}...
    };
newPlot.errorBars='line';
newPlot.limits.time=[-12,12];
newPlot.limits.nbOfMolecules=[0,1000];
newPlot.limits.speed=[0,0.4];
newPlot.limits.distance=[0,0.9];
newPlot.limits.assemblyRate=[];
newPlot.ticks.time=-10:2:10;
newPlot.ticks.nbOfMolecules=0:200:1000;
newPlot.ticks.speed=0:0.05:0.4;
newPlot.ticks.distance=0:0.1:0.9;
plots=[plots newPlot];

% 3rd group of plots
newPlot=struct;
newPlot.name='allTracksPil1DFimbrin';
newPlot.strainsToPlot={'fim1GFP_pil1D'}; % e.g. {'fim1GFP_WT','fim1GFP_pil1D'}
newPlot.figures={...
    {'time','nbOfMolecules'},... % e.g. {'time','nbOfMolecules'} or {'speed','nbOfMolecules'}
    {'time','speed'},...
    };
newPlot.individualTracksToPlot={...
    {'fim1GFP_pil1D','all'}...
    }; % First argument is the strain name, second is an array of indices or 'all' to plot all
newPlot.errorBars='regular';
newPlot.limits.time=[-12,12];
newPlot.limits.nbOfMolecules=[0,1000];
newPlot.limits.speed=[0,0.4];
newPlot.limits.distance=[0,0.9];
newPlot.limits.assemblyRate=[-400,400];
newPlot.ticks.time=-10:2:10;
newPlot.ticks.nbOfMolecules=0:200:1000;
newPlot.ticks.speed=0:0.05:0.4;
newPlot.ticks.distance=0:0.1:0.9;
newPlot.ticks.assemblyRate=[-1:0.5:1]*400;
plots=[plots newPlot];

%%% Template for another group of plots
% newPlot=struct; % Initialisation of the first group of plots
% newPlot.name=''; % id of the group of plots
% newPlot.strainsToPlot={'',''};% identifiers of the strains to plot (e.g. {'fim1GFP_in_WT','fim1GFP_pil1D'}). Strains need to defined in 'strains'
% newPlot.figures={... % list of figures to plot. Possible data identifiers are: time, nbOfMolecules, distance, speed, assemblyRate, MSD, pathLength
%     {'',''},... % pair of data identifiers to plot versus each other (e.g. {'time','nbOfMolecules'})
%     {'',''},...
%     {'',''},...
%     };
% newPlot.individualTracksToPlot={... % (optional) List of individual datasets (from individual patches) to plot.
%     {'',''},... % First argument is the strain name, second is an array of indices or 'all' to plot all tracks
%     {'',''}...
%     };
% newPlot.errorBars=''; % type of error bars/lines to plot. Possible values: regular, line, markers, line_and_markers, standard_error, confidenceInterval95pc , none
% newPlot.limits.time=[,]; % limits for the time axis (optional)
% newPlot.limits.nbOfMolecules=[,];
% newPlot.limits.speed=[,];
% newPlot.limits.distance=[,];
% newPlot.limits.assemblyRate=[,];
% newPlot.ticks.time=::; % ticks on time axis (optional)
% newPlot.ticks.nbOfMolecules=::;
% newPlot.ticks.speed=::;
% newPlot.ticks.distance=::;
% newPlot.ticks.assemblyRate=::;
% plots=[plots newPlot]; % Addition of the group of plots to the list (Do not delete)


%% Other parameters for temporal super-resolution alignment, plotting styles, saving, etc
% 'pathToSaveFigures' (optional): folder where the figures will be saved in pdf format (editable with Illustrator or Inkscape). Figures will not be saved if omitted
% 'reduceSymbolDensityBy' (optional): the density of symbols on figures will be divided by this number
% The following parameters are for temporal super-resolution realignment and averaging:
%	'nbConsecutiveContinuousRealignements': number of consecutive runs of the algorithm, typically between 3 and 7 (run 1, realigns on a track with median length (picked randomly), run 2 and following, realigns on the average dataset from previous run
%   'timeForContinuousAverage': time frame to use for realignment and interpolations(e.g.[0,0.1,40])
%   'precision': precision of the temporal super-resolution realignment in seconds (typically 0.1 to 0.5)
%   'scoreXStepSize': time step used for the calculation of the mean square difference between 2 datasets (typically 0.5 or around the sampling time step)
%	'minNbDataPointsForAverages': average values where less than this number of points are used to calculate the average will not be shown on the plot (typically at least 3 to 5)
otherParameters={}; % Initialisation
otherParameters.nbConsecutiveContinuousRealignements=7; % number of consecutive runs of the algorithm, typically between 3 and 7
otherParameters.scoreXStepSize=0.5; % time step used for the calculation of the mean square difference between 2 datasets (typically 0.5 or around the sampling time step)
otherParameters.precision=0.1; % precision of the temporal super-resolution realignment in seconds (typically 0.1 to 0.5)
otherParameters.timeForContinuousAverage=0:0.1:40; % time frame to use for realignment and interpolations(e.g.[0:0.1:40])
otherParameters.minNbDataPointsForAverages=5; % average values where less than this number of points are used to calculate the average will not be shown on the plot (typically 5 or larger)
otherParameters.reduceSymbolDensityBy=10; % the density of symbols on figures will be divided by this number
otherParameters.pathToSaveFigures='C:\Users\jb\Desktop\Joels data\Delta Pil1 Fim1\Matlab figures\';



%% Do not edit beyond this point!
% Construction of the 'experiment' structure
experiments=struct;
experiments.aliases=aliases;
experiments.defaultValues=defaultValues;
experiments.strains=strains;
experiments.plots=plots;
experiments.otherParameters=otherParameters;