function [txtDataToPlot]=whichData(dataToPlot)
% whichData:
% Transforms a data ID into text
%
%
% Julien Berro
% 2012-2016
% Yale University

global DATA_DISTANCE DATA_PATHLENGTH DATA_MSD DATA_SPEED DATA_CONCENTRATION DATA_ASSEMBLYRATE;
switch dataToPlot
    case DATA_DISTANCE
        txtDataToPlot='Distance';
    case DATA_PATHLENGTH
        txtDataToPlot='Path Length';
    case DATA_MSD
        txtDataToPlot='MSD';
    case DATA_SPEED
        txtDataToPlot='Speed';
    case DATA_CONCENTRATION
        txtDataToPlot='Concentration';
    case DATA_ASSEMBLYRATE 
        txtDataToPlot='Assembly Rate';
end
end