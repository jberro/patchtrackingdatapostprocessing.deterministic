function [returnExpData,fuorecentProteinIDs,mutantIDs,offSetForFinalPlot,exp]=loadPatchDataJSONFile(dataDescriptionFilePath)
% DEPRECATED
% loadPatchDataJSONFile:
% Reads a JSON format file containing information about the data to load
% and the figures to plot and populates an ExperimentDescriptionJSONFile object
%
% Julien Berro
% 2012-2016
% Yale University

fprintf('....Loading data described in \"%s\"\n',dataDescriptionFilePath);
exp=ExperimentDescriptionJSONFile(dataDescriptionFilePath);
returnExpData=exp.allDatasetsInFPxMutantCell;
fuorecentProteinIDs=1:length(exp.allFPNames);
mutantIDs=1:length(exp.allMutantNames);
offSetForFinalPlot=exp.allTimePeak;
end