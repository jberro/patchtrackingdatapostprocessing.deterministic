%% ToolboxAlignment.m
% Julien Berro
% Yale University
% 7/7/2013
% Class containing the functions necessary to apply the continuous
% alignment method (aka "temporal super-resolution" alignment)
% Usage: create an object from the class ToolboxAlignment and run the
% function getContinuousBestAlignment on the datasets to realign
% Requires: the global variables SCORE_NOT_NORMALIZED SCORE_NORMALIZED to
% be set    

classdef ToolboxAlignment< handle
    properties
    end
    
    methods
        function obj = ToolboxAlignment()
        end
        
        function [bestContinuousScore,bestContinuousOffset,scoreFunction,functionRefArray,functionMyArray,score,offSets]=...
                getContinuousBestAlignment(obj,refArray,xRefArray,myArray,xMyArray,first,last,scoreXStepSize,precision,interpolationMethod,spillover,scoreNormalisationMode,normalizeData)
            if nargin==12
                normalizeData=false;
            end
            
            %% Calculates the best alignment for the 2 arrays 
            % The alignment aligns myArray to refArray. It assumes refArray is larger
            %  than my array and myArray is slided relatively to refArray. The number of
            %  allowed non-overlaping datapoints is controlled  by the variable
            %  spillover (number of datapoints at the begin or the end of one dataset will not have any
            %  matching points in the other dataset)
            %     'refArray' and 'xRefArray' contain respectively the y data and the x data of the reference track (typically the longest track or the average track if several realignment rounds are performed)
            %     'myArray and 'xMyArray' contain respectively the y data and the x data of the track to align to refArray
            %     'first' and 'last' define the interval in refArray myArray will be compared to
            %     'scoreXStepSize' is the stepsize between consecutive points (in x) that are used to evaluate the distance (or RMSD) between both arrays
            %     'precision' is the stepsize used to find the minimum score. This value is the precision of the computed best offset.
            %     'interpolationMethod' is the method used to interpolate the data from myArray and refArray. Possible methods are the same as for the interp1 function ('linear','spline','cubic',...)
            %     'spillover': size of the interval of datapoints at the begin or the end of one dataset will not have any matching points in the other dataset)amount of data of myArray that are allowed to not match any data in refArray. WARNING: it is given in the same scale as xMyArray 
            %                 For example, if xMyArray==0:0.01:2 and spillover==0.05, datasets will be shifted over intervals included in -0.5:0.01:2.05  
            %     'scoreNormalisationMode' (value: either one of the global variables SCORE_NOT_NORMALIZED or SCORE_NOT_NORMALIZED): 
            %            SCORE_NOT_NORMALIZED: Do not normalize -> favors peak values (scoreMode~=1)
            %            SCORE_NORMALIZED: Normalize the scores for each datapoint
            %     'normalizeData' (value:boolean, can be omited, default: 'false'): true if datasets has to be normalized to peak value before realignment
            %
            % Returns:
            %     'bestContinuousScore': minimal mean square difference between myArray and refArray
            %     'bestContinuousOffset': offset that minimizes the mean square difference between myArray and refArray. myArray has to be moved by bestContinuousOffset.
            %     'scoreFunction': function that retruns the mean square difference between myArray and refArray for a given offset.
            %     'functionRefArray': interpolated function based on the data in refArray and xRefArray, using the method interpolationMethod. The value of the function is 0 outside the limits of xRefArray.
            %     'functionMyArray': interpolated function based on the data in myArray and xMyArray, using the method interpolationMethod. The value of the function is 0 outside the limits of xMyArray.
            %     'score': (minimal) score corresponding to best alignment
            %     'offSets': offsets for the best alignments, ie offsets that minimize the score. Note: this is not necessarily a unique value (usually it is)
            %
            
            debugMode=false;
            
            %% transpose arrays if they don't have the right dimensions
            % refArray, myArray should be column vectors (size==(n,1))
            % xRefArray, xMyArray are row vector (size==(1,n))
            sizeRef=size(refArray);
            sizeMy=size(myArray);
            sizeXRef=size(xRefArray);
            sizeXMy=size(xMyArray);
            if(sizeRef(1,2)~=1)
                refArray=refArray';
            end
            if(sizeMy(1,2)~=1)
                myArray=myArray';
            end
            if(sizeXRef(1,1)~=1)
                xRefArray=xRefArray';
            end
            if(sizeXMy(1,1)~=1)
                xMyArray=xMyArray';
            end
            
            %updates dimensions
            sizeRef=size(refArray,1);
            sizeMy=size(myArray,1);
            sizeXRef=size(xRefArray,2);
            sizeXMy=size(xMyArray,2);
            
            
            % if size(myArray)~=size(xMyArray')
            % end
            
            %% Remove NaN values
            indexNotNaN=find(~isnan(refArray));
            refArray=refArray(indexNotNaN);
            xRefArray=xRefArray(indexNotNaN);
            indexNotNaN=find(~isnan(myArray));
            myArray=myArray(indexNotNaN);
            xMyArray=xMyArray(indexNotNaN);
            
            %% Renames arrays (inherited from older version...)
            refArrayToUse=refArray;
            xRefArrayToUse=xRefArray;
            myArrayToUse=myArray;
            xMyArrayToUse=xMyArray;
                     
            %% Normalize data if needed
            if normalizeData
                 refArrayToUse=refArrayToUse/max(refArrayToUse);
                 myArrayToUse=myArrayToUse/max(myArrayToUse);
            end
            
            sizeRef=size(refArray,1);
            sizeMy=size(myArray,1);
            sizeXRef=size(xRefArray,2);
            sizeXMy=size(xMyArray,2);
            %% Dumb proofing for first and last index. Also allows to use -intmax and +intmax to use the entire track
            if(first<1)
                first=1;
            end
            if(last>sizeRef)
                last=sizeRef;
            end
            
            %% Calculation of the interval of possible offsets
            % myArray can be moved to the left so that its first value matches the first value of
            %   refArray, and to the right so that its last value matches the last value
            %   of refArray. It can be moved more, controlled by the value of spillover
            xRefArrayAmplitude=xRefArray(last)-xRefArray(first);
            xMyArrayAmplitude=xMyArray(sizeMy)-xMyArray(1);
            % minOffSet=xRefArray(first)-xMyArray(1)-spillover;
            % maxOffSet=xRefArray(last)-xMyArray(sizeXMy)+spillover;
            minOffSet0=xRefArray(first)-xMyArray(1);
            maxOffSet0=xRefArray(last)-xMyArray(sizeXMy);
            % In the case of myArray lasting longer than xRefArray, maxOffset0 will be negative and we need to swap min and max
            minOffSet=min(minOffSet0,maxOffSet0)-spillover;
            maxOffSet=max(minOffSet0,maxOffSet0)+spillover;
            xScore=xRefArray(first):scoreXStepSize:xRefArray(last);
            
            %% Creates interpolation functions and score function
            % Value is 0 outside the xRefArray and xMyArray
            % The score function is the mean square difference between both interpolated functions
            functionRefArray=@(t) obj.functionRefArrayLocal(t,xRefArrayToUse,refArrayToUse,interpolationMethod);
            functionMyArray=@(t) obj.functionMyArrayLocal(t,xMyArrayToUse,myArrayToUse,interpolationMethod);
            scoreFunction = @(t) obj.scoreFunctionLocal(t,xRefArrayToUse,xMyArrayToUse,first,last,xScore,functionRefArray,functionMyArray,scoreNormalisationMode);
            xMinimisation=minOffSet:precision:maxOffSet;
            
            %% Compute the score and the best offset
            score=scoreFunction(xMinimisation);
            bestContinuousScore=min(score);
            ind=find(score==bestContinuousScore);
            if size(ind,1)==0
            end
            bestContinuousOffset=xMinimisation(ind(1,1));
            
            %% debug Mode
            % if debugMode
            %     % %     bestContinuousOffset
            %     %     figure(5)
            %     %     clf
            %     %     plot(xMinimisation,scoreFunction(xMinimisation))
            %     figure(6)
            %     clf; hold on;
            %     %refArray,xRefArray,myArray,xMyArray
            %     minXRef=min(xRefArray);
            %     maxXRef=max(xRefArray);
            %     deltaXRef=maxXRef-minXRef;
            %     minXMy=min(xMyArray);
            %     maxXMy=max(xMyArray);
            %     deltaXMy=maxXMy-minXMy;
            %     maxDelta=max(deltaXMy,deltaXRef);
            %     minMin=min(minXMy,minXRef);
            %     maxMax=max(maxXMy,maxXRef);
            %     xplot=(minMin-maxDelta):0.1:((maxMax+maxDelta));
            %     %   plot(xplot,functionRefArray(xplot),'r')
            %     plot(xRefArrayToUse,refArrayToUse,'r.');
            %     %   plot(xplot,functionMyArray(xplot),'g')
            %     plot(xMyArrayToUse,myArrayToUse,'g.');
            %
            %     %plot(xplot+bestContinuousOffset,functionMyArray(xplot),'b')
            %
            %     %     figure(7)
            %     %     clf; hold on;
            %     %    plot(xplot,scoreFunction(xplot),'b')
            %     %
            % end %debugMode
            %%
            offSets=xMinimisation;
        end
        
        
        function score=scoreFunctionLocal(obj,t,xRefArrayToUse,xMyArrayToUse,first,last,xScore,functionRefArray,functionMyArray,scoreNormalisationMode)
            %% Creates score function (mean square difference between the functions that interpolates xRefArrayToUse and xMyArrayToUse
            global SCORE_NOT_NORMALIZED SCORE_NORMALIZED;
            sizeXRef=size(xRefArrayToUse,2);
            sizeXMy=size(xMyArrayToUse,2);
            min0=xMyArrayToUse(1);
            max0=xMyArrayToUse(sizeXMy);
            [in,sizeRange]=obj.inRange(obj.myadd(xScore,-t),min0,max0);
            if scoreNormalisationMode==SCORE_NORMALIZED
                % Original: normalized to the reference values
                %score=sum(in.*((functionMyArray(obj.myadd(xScore,-t))-obj.myadd(functionRefArray(xScore),zeros(size(t)))).^2)./obj.myadd(functionRefArray(xScore),zeros(size(t))).^2,2)./sizeRange;
                % Log normalisation
                arrayLogs=in.*((log(abs(functionMyArray(obj.myadd(xScore,-t))))-obj.myadd(log(abs(functionRefArray(xScore))),zeros(size(t)))).^2);
                indicesInf=(isnan(arrayLogs));
                arrayLogs(indicesInf)=0;
                score=sum(arrayLogs,2)./sizeRange;
            else
                score=sum(in.*((functionMyArray(obj.myadd(xScore,-t))-obj.myadd(functionRefArray(xScore),zeros(size(t)))).^2),2)./sizeRange;
            end
        end
        
        function res=myadd(obj,t,u)
            %% Creates a matrix where the jth lines corresponds to the sum of t and the jth value of u
            res=ones(size(u,2),size(t,2))*diag(t)+diag(u)*ones(size(u,2),size(t,2));
        end
        
        function fun=functionRefArrayLocal(obj,t,xRefArrayToUse,refArrayToUse,interpolationMethod)
            fun=interp1(xRefArrayToUse,refArrayToUse,t,interpolationMethod,0);
        end
        
        function fun=functionMyArrayLocal(obj,t,xMyArrayToUse,myArrayToUse,interpolationMethod)
            fun=interp1(xMyArrayToUse,myArrayToUse,t,interpolationMethod,0);
        end
        
        function maxArray=maxSingleValueAndArray(obj,singleValue,arrayOfValues)
            %% Returns an array with the max of singleValue and each element of arrayOfValues
            sizeArray=size(arrayOfValues,2);
            maxArray=zeros(1,sizeArray);
            for i=1:sizeArray
                maxArray(1,i)=max(singleValue,arrayOfValues(1,i));
            end
        end
        
        function minArray=minSingleValueAndArray(obj,singleValue,arrayOfValues)
            %% Returns an array with the min of singleValue and each element of arrayOfValues
            sizeArray=size(arrayOfValues,2);
            minArray=zeros(1,sizeArray);
            for i=1:sizeArray
                minArray(1,i)=min(singleValue,arrayOfValues(1,i));
            end
        end
        
        
        function [in,nInRange]=inRange(obj,t,min0,max0)
            %% t is a n x m array ; min0 and max0 are 1 x 1 arrays
            %% Returns an array of booleans the same size as t, in(i,j)=1 if min0<=t(i,j)<=max0
            sizeT=size(t);
            in=zeros(sizeT);
            nInRange=zeros(sizeT(1),1);
            for i=1:sizeT(1)
                %     in(i,:)=(t(i,:)>=min0(i)).*(t(i,:)<=max0(i));
                in(i,:)=(t(i,:)>=min0).*(t(i,:)<=max0);
                nInRange(i)=size(find(in(i,:)==1),2);
            end
        end
        
    end
end