function newPlotIds=reduceMarkerDensity(plotID,markerDensity)
% reduceMarkerDensity:
% Reduces the marker density by a factor markerDensity in already existing plots (plotID) 
%
% Julien Berro
% 2012-2016
% Yale University

newPlotIds=zeros(size(plotID));
counter=1;
for curPlotID=plotID
    propertiesToTransfert={...
        'Color',...
        'LineWidth',...
        'Marker',...
        'MarkerSize',...
        'MarkerEdgeColor',...
        'MarkerFaceColor'};
    curPlotProperties=get(curPlotID,propertiesToTransfert);
    set(curPlotID,'Marker','none');
    x=get(curPlotID,'XData');
    y=get(curPlotID,'YData');
    n=length(x);
    newIndices=1:markerDensity:n;
    newX=x(newIndices);
    newY=y(newIndices);
    h=plot(newX,newY,'*r');
    set(h,propertiesToTransfert,curPlotProperties);
    newPlotIds(counter)=h;
    counter=counter+1;
end
%uistack(newPlotIds,'bottom');
end
                