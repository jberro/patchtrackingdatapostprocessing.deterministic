function [returnExpData,fuorecentProteinIDs,mutantIDs,offSetForFinalPlot,exp]=loadPatchDataMatlabFile(dataDescriptionFilePath)
% loadPatchDataMatlabFile:
% Reads a Matlab file containing information about the data to load
% and the figures to plot, and populates an ExperimentDescriptionMatlabFile object
%
% Julien Berro
% 2012-2016
% Yale University

fprintf('....Loading data described in \"%s\"\n',dataDescriptionFilePath);
exp=ExperimentDescriptionMatlabFile(dataDescriptionFilePath);
returnExpData=exp.allDatasetsInFPxMutantCell;
fuorecentProteinIDs=1:length(exp.allFPNames);
mutantIDs=1:length(exp.allMutantNames);
offSetForFinalPlot=exp.allTimePeak;
end