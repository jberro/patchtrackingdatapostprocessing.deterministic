function [bestScore,bestOffset]=getBestAlignment(refArray,myArray,first,last,maxOffset,minNbEval,scoreMode)
% getBestAlignment:
% Calculates the best alignment for the 2 arrays with "regular" temporal resolution
% minNbEval: nb of overlapping points
%
% Julien Berro
% 2012-2016
% Yale University

sizeRef=size(refArray,1);
sizeMyArray=size(myArray,1);
realMaxOffSet=min(maxOffset,sizeMyArray);
bestScore=realmax;
bestOffset=intmax;
for offset=-realMaxOffSet:realMaxOffSet
    [curScore,nbEval]=scoreArrays(refArray,myArray,first,last,offset,minNbEval,scoreMode);
    if (curScore<bestScore)
        bestScore=curScore;
        bestOffset=offset;
    end
end
if bestOffset==intmax
    sprintf('Warning: bestOffset cannot be computed in getBestAlignment. Check the arrays, the offsets and minNbEval')
    refArray
    myArray
    bestScore
    bestOffset
end

end
