function [score,nbEval]=scoreArrays(refArray,myArray,first,last,offset,minNbEval,scoreMode)
    % scoreArrays:
    % Shift myArray with offset then compares with refArray on the overlap, 
    % if none of the timepoints are 0 or NaN. There should be at least
    % minNbEval overlapping points
    % first and last define the interval in refArray myArray will be
    % compared to 
    %
    %
    % Julien Berro
    % Yale University
    % 2012-2016
    
    sizeMyArray=size(myArray,1);
    score=0;
	goodScore=false;
	nbEval=0;
    sizeRef=size(refArray,1);
    sizeMyArray=size(myArray,1);
    shiftedMyArrayFirst=1+offset;
    shiftedMyArrayLast=sizeMyArray+offset;
    intersectionFirst=max(first,shiftedMyArrayFirst); % in refArrayCoordinates
    intersectionLast=min(last,shiftedMyArrayLast);
    for i=intersectionFirst:intersectionLast
        if (refArray(i,1)~=0&&myArray(i-offset,1)~=0&&~isnan(refArray(i,1))&&~isnan(myArray(i-offset,1)))
            if (scoreMode==1) % Normalize the scores for each datapoint
                score=score+((refArray(i,1)-myArray(i-offset,1))/refArray(i,1))^2;
            else % Do not normalize -> favors peak values
                score=score+(refArray(i,1)-myArray(i-offset,1))^2;
            end
            nbEval=nbEval+1;
        end
    end
	
    %startsAt=max(first,1+offset)
    %endsAt=min(last,sizeMyArray+offset)
    %for i=1:endsAt-startsAt+1
	%	if (refArray(i+offset,1)~=0&&myArray(i,1)~=0&&~isnan(refArray(i+offset,1))&&~isnan(myArray(i,1)))
	%		%refArray(i,1)
    %        %myArray(i+offset,1)
    %        score=score+(refArray(i+offset,1)-myArray(i,1))^2;
	%		%score=score+((refArray(i+offset,1)-myArray(i,1))/refArray(i+offset,1))^2;
	%		goodScore=true;
	%		nbEval=nbEval+1;
    %    end
    %end
	%if (goodScore==false||nbEval<minNbEval)

    if ((nbEval<minNbEval)) %% &&((last-first+1)>minNbEval)) % second condition to let small arrays be scored
        score=realmax;
    else
		score=score/nbEval;
    end
end