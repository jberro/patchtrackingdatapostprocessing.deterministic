classdef ExperimentDescriptionMatlabFile < handle
    % ExperimentDescriptionMatlabFile: 
    % class representing information contained in the Matlab format file used
    % to describe the data to load and the figures to plot. 
    %
    % Julien Berro
    % 2012-2016
    % Yale University
    
        properties
        path
        allStrainNames
        allMutantNames
        allMutantsStruct
        allFPNames
        allFPsStruct
        allAliases
        allDefaultValues
        allExpData
        allDatasetsInFPxMutantCell
        allTimePeak
        allPlots
        allPlotNames
        dataStructure
        pathToSaveFigures
        
        reduceSymbolDensityBy
        
    end
    
    methods
        function obj = ExperimentDescriptionMatlabFile(path)
            obj.path=path;
            obj.loadExperimentDescriptionFile(path);
        end
        
        function obj = loadExperimentDescriptionFile(obj,dataDescriptionFilePath)
            run(dataDescriptionFilePath);
            obj.dataStructure=experiments;
            
            % Convert dataStructure.strains from a cell into a structure
            structStrains=struct;
            for i=1:length(obj.dataStructure.strains)
                structStrains.(obj.dataStructure.strains{i}.name)=obj.dataStructure.strains{i};
            end 
            obj.dataStructure.strains=structStrains;
            
            % Find all mutants and Fluorescent proteins
            obj.allStrainNames=fieldnames(obj.dataStructure.strains);
            nStrains=length(obj.allStrainNames);
            obj.allMutantNames=cell(nStrains,1);
            obj.allFPNames=cell(nStrains,1);
            for i=1:nStrains
                obj.allMutantNames{i}=obj.dataStructure.strains.(obj.allStrainNames{i}).mutant;
                obj.allFPNames{i}=obj.dataStructure.strains.(obj.allStrainNames{i}).fluorescentProtein;
            end
            obj.allMutantNames=unique(obj.allMutantNames);
            obj.allFPNames=unique(obj.allFPNames);
            
            % create global variables from mutant and FP names
            global allMutantsStruct;
            allMutantsStruct=struct();
            for i=1:length(obj.allMutantNames)
                allMutantsStruct.(obj.allMutantNames{i})=i;
                %    eval(['global ' obj.allMutantNames{i} '; ' obj.allMutantNames{i} '=' num2str(i) ';']);
            end
            obj.allMutantsStruct=allMutantsStruct;
            
            global allFPsStruct;
            allFPsStruct=struct();
            for i=1:length(obj.allFPNames)
                allFPsStruct.(obj.allFPNames{i})=i;
                %    eval(['global ' obj.allFPNames{i} '; ' obj.allFPNames{i} '=' num2str(i) ';']);
            end
            obj.allFPsStruct=allFPsStruct;
            
            % find all aliases and convert into a structure
            obj.allAliases={};
            for i=1:length(obj.dataStructure.aliases)
                obj.allAliases.(obj.dataStructure.aliases{i}{1})=obj.dataStructure.aliases{i}{2};
            end
            
            % find all default values
            obj.allDefaultValues=obj.dataStructure.defaultValues;
            
            %Construction of data objects
            obj.allExpData=cell(length(obj.allStrainNames),1);
            obj.allDatasetsInFPxMutantCell=cell(length(obj.allStrainNames),1);
            for i=1:length(obj.allStrainNames)
                obj.allDatasetsInFPxMutantCell{i}=cell(length(obj.allMutantNames),1);
            end
            for i=1:length(obj.allStrainNames)
                %curStrain=obj.dataStructure.strains.(obj.allStrainNames{i});
                curStrain=obj.dataStructure.strains.(obj.allStrainNames{i});
                pathsTmp=curStrain.paths;
                curExperimentData=importData(pathsTmp{1},obj.allStrainNames{i});
                curExperimentData.mutant=obj.allMutantsStruct.(curStrain.mutant);
                curExperimentData.fluorescentProtein=obj.allFPsStruct.(curStrain.fluorescentProtein);
                obj.assignParameterIfDefined(curExperimentData,'textToPrintMutant',curStrain,'textToPrintMutant');
                obj.assignParameterIfDefined(curExperimentData,'textToPrintFluorescentProtein',curStrain,'textToPrintFluorescentProtein');
                obj.assignParameterIfDefined(curExperimentData,'exposureTimeInMs',curStrain,'exposureTimeInMs');
                obj.assignParameterIfDefined(curExperimentData,'AUperMoleculePerSecond',curStrain,'calibrationAUperMoleculePerSecond');
                obj.assignParameterIfDefined(curExperimentData,'color',curStrain,'color');
                obj.assignParameterIfDefined(curExperimentData,'colorErrorBars',curStrain,'colorErrorBars');
                obj.assignParameterIfDefined(curExperimentData,'symbol',curStrain,'symbol');
                obj.assignParameterIfDefined(curExperimentData,'delayFrames',curStrain,'delayFramesInSec');
                obj.assignParameterIfDefined(curExperimentData,'timePeak',curStrain,'timePeak');
                obj.allTimePeak{curExperimentData.fluorescentProtein}{curExperimentData.mutant}=curExperimentData.timePeak;
                obj.allExpData{i}=curExperimentData;
                if isfield(curStrain,'relativeSymbolSize')
                    curExperimentData.relativeMarkerSize=curStrain.('relativeSymbolSize');
                else
                    curExperimentData.relativeMarkerSize=1;
                end
                
                obj.allDatasetsInFPxMutantCell{curExperimentData.fluorescentProtein}{curExperimentData.mutant}=obj.allExpData{i};
                for j=2:length(pathsTmp)
                    expDatatmp=importData(pathsTmp{j},obj.allStrainNames{i});
                    obj.allExpData{i}.roisets=[obj.allExpData{i}.roisets expDatatmp.roisets ];
                    obj.allExpData{i}.filePaths=char(obj.allExpData{i}.filePaths,expDatatmp.filePaths);
                end
            end
            
            % Construction of all plots
            nPlots=length(obj.dataStructure.plots);
            obj.allPlotNames=cell(nPlots,1);
            obj.allPlots=cell(nPlots,1);
            for i=1:nPlots
                obj.allPlotNames{i}=obj.dataStructure.plots{i}.name;
                obj.allPlots{i}=obj.dataStructure.plots{i};
                % Transform individual plots to track into a structure
                if isfield(obj.dataStructure.plots{i},'individualTracksToPlot')
                    structureIndividualTracksToPlot={};
                    for j=1:length(obj.dataStructure.plots{i}.individualTracksToPlot)
                        structureIndividualTracksToPlot.(obj.dataStructure.plots{i}.individualTracksToPlot{j}{1})=obj.dataStructure.plots{i}.individualTracksToPlot{j}{2};
                    end
                    obj.allPlots{i}.individualTracksToPlot=structureIndividualTracksToPlot;
%                     if isfield(plotStructure.individualTracksToPlot,curStrainname)
                end
            end
            
            % Setup of other parameters
            if isfield(obj.dataStructure.otherParameters,'pathToSaveFigures')
                obj.pathToSaveFigures=obj.dataStructure.otherParameters.pathToSaveFigures;
            else
                obj.pathToSaveFigures='';
            end
            if isfield(obj.dataStructure.otherParameters,'reduceSymbolDensityBy')
                obj.reduceSymbolDensityBy=obj.dataStructure.otherParameters.reduceSymbolDensityBy;
            else
                obj.reduceSymbolDensityBy=1;
            end
            
        end
        
        function id=convertVariableNameIntoID(obj,name)
            global DATA_TIME DATA_DISTANCE DATA_PATHLENGTH DATA_MSD DATA_SPEED DATA_CONCENTRATION DATA_ASSEMBLYRATE;
            switch name
                case 'time'
                    id=DATA_TIME;
                case 'nbOfMolecules'
                    id=DATA_CONCENTRATION;
                case 'distance'
                    id=DATA_DISTANCE;
                case 'speed'
                    id=DATA_SPEED;
                case 'assemblyRate'
                    id=DATA_ASSEMBLYRATE;
                case 'MSD'
                    id=DATA_MSD;
                case 'pathLength'
                    id=DATA_PATHLENGTH;
            end
        end
        
        function [dataToPlotVsTime,titleFigureVsTime,correlationPlots,titleFigureCorrelation,stdMode,...
                limits,ticks,strainsToPlot,indicesIndividualTracksToPlot,colorsIndividualTracksToPlot]=...
                getAllPlotInfos(obj,plotStructure)
            global STD_NONE;
% %             offSetFigureStyle=0;
% %             nb=nb+1;
% %         textMutantCell{nb}='';
% % colorsIndividualTracksToPlot=colormap(jet(length(indicesIndividualTracksToPlot)));
% %   if length(indicesIndividualTracksToPlot)==0 plotAllDatapoints=false;
% % if path assigned : saveFigure3=false;

            if isfield(plotStructure,'errorBars')
                stdMode=obj.convertErrorBarMode(obj.changeIntoAliasValueIfExists(plotStructure.errorBars));
            else
                stdMode=STD_NONE;
            end
            dataToPlotVsTime=[];
            titleFigureVsTime=[];
            correlationPlots=[];
            titleFigureCorrelation=[];
            
            for i=1:size(plotStructure.figures,2)
                if strcmp(plotStructure.figures{i}{1},'time')
                    dataToPlotVsTime=[dataToPlotVsTime obj.convertVariableNameIntoID(plotStructure.figures{i}{2})];
                    titleFigureVsTime=char(titleFigureVsTime,plotStructure.figures{i}{2});
                else
                    correlationPlots=[correlationPlots; obj.convertVariableNameIntoID(plotStructure.figures{i}{1}) obj.convertVariableNameIntoID(plotStructure.figures{i}{2})];
                    titleFigureCorrelation=char(titleFigureCorrelation,[plotStructure.figures{i}{2} ' vs ' plotStructure.figures{i}{1}]);
                end
            end
            titleFigureVsTime=titleFigureVsTime(2:size(titleFigureVsTime,1),:);
            titleFigureCorrelation=titleFigureCorrelation(2:size(titleFigureCorrelation,1),:);
            
            % set limits and ticks
            tmpVariables={'time', 'nbOfMolecules', 'distance', 'speed', 'assemblyRate', 'MSD', 'pathLength'};
            limits=cell(1,7);
            ticks=cell(1,7);
            for j=1:length(tmpVariables)
                curVariable=tmpVariables{j};
                id=obj.convertVariableNameIntoID(curVariable);
                if isfield(plotStructure.limits,curVariable)
                    limits{id}=plotStructure.limits.(curVariable);
                else
                    limits{id}=[];
                end
                if isfield(plotStructure.ticks,curVariable)
                    ticks{id}=plotStructure.ticks.(curVariable);
                else
                    ticks{id}=[];
                end
            end
            
            % Set strains to plot
            strainsToPlot=obj.buildArrayOfStrainIDsFromStrainNames(plotStructure.strainsToPlot);
            
            % Set individual strains to plot
            indicesIndividualTracksToPlot={};
            colorsIndividualTracksToPlot={};
            for ll=1:length(plotStructure.strainsToPlot)
                curStrainname=plotStructure.strainsToPlot{ll};
                idSrain=obj.buildArrayOfStrainIDsFromStrainNames({curStrainname});
                curIndices=[];
                if isfield(plotStructure,'individualTracksToPlot')
                    if isfield(plotStructure.individualTracksToPlot,curStrainname)
                        valueCurInd=plotStructure.individualTracksToPlot.(curStrainname);
                        if (isstr(valueCurInd) && strcmp(valueCurInd,'all'))
                            curIndices=1:length(obj.allDatasetsInFPxMutantCell{idSrain(1)}{idSrain(2)}.roisets);
                        else
                        	curIndices=valueCurInd;
                        end
                    end
                end
                indicesIndividualTracksToPlot{idSrain(1)}{idSrain(2)}=curIndices;
                colorsIndividualTracksToPlot{idSrain(1)}{idSrain(2)}=colormap(jet(length(curIndices)));
            end

        end
        

        
        function assignParameterIfDefined(obj,experimentData,experimentDataParam,jsonStruct,jsonParam)
            if isfield(jsonStruct,jsonParam) % if defined
                experimentData.(experimentDataParam)= obj.changeIntoAliasValueIfExists(jsonStruct.(jsonParam));
            else % if not, try to find default value
                defaultParamName=['default' upper(jsonParam(1)) jsonParam(2:length(jsonParam))];
                if isfield(obj.allDefaultValues,defaultParamName)
                    experimentData.(experimentDataParam)= obj.changeIntoAliasValueIfExists(obj.allDefaultValues.(defaultParamName));
                else
                    fprintf(['WARNING! ' experimentData.name ' : ' jsonParam ' is undefined\n']);
                end
            end
        end
        
        function [finalValue] = changeIntoAliasValueIfExists(obj,value)
            if isfield(obj.allAliases,value)
                finalValue=obj.allAliases.(value);
            else
                finalValue=value;
            end
        end
        
        function arrayOfIndices = buildArrayOfStrainIDsFromPairs(obj, cellArrayOfStrains)
            arrayOfIndices = zeros(size(cellArrayOfStrains));
            for i=1:size(cellArrayOfStrains,1)
                arrayOfIndices(i,1)=obj.allFPsStruct.(cellArrayOfStrains{i,1});
                arrayOfIndices(i,2)=obj.allMutantsStruct.(cellArrayOfStrains{i,2});
            end
        end
        
        function arrayOfIndices = buildArrayOfStrainIDsFromStrainNames(obj, cellArrayOfStrainNames)
            arrayOfIndices = zeros(length(cellArrayOfStrainNames),2);
            for i=1:length(cellArrayOfStrainNames)
                arrayOfIndices(i,1)=obj.allFPsStruct.(obj.dataStructure.strains.(cellArrayOfStrainNames{i}).fluorescentProtein);
                arrayOfIndices(i,2)=obj.allMutantsStruct.(obj.dataStructure.strains.(cellArrayOfStrainNames{i}).mutant);
            end
        end
        
        function stdMode = convertErrorBarMode(obj,errorBarMode)
            global STD_NONE STD_REGULAR STD_MARKERS STD_LINE STD_LINE_AND_MARKERS STD_STANDARD_ERROR STD_CONFIDENCE_95PC
            STD_NONE=1;
            STD_REGULAR=2;
            STD_MARKERS=3;
            STD_LINE=4;
            STD_LINE_AND_MARKERS=5;
            STD_STANDARD_ERROR=6;
            STD_CONFIDENCE_95PC=7;
            switch errorBarMode
                case 'regular'
                    stdMode=STD_REGULAR;
                case 'line'
                    stdMode=STD_LINE;
                case 'markers'
                    stdMode=STD_MARKERS;
                case 'line_and_markers'
                    stdMode=STD_LINE_AND_MARKERS;
                case 'standard_error'
                    stdMode=STD_STANDARD_ERROR;
                case 'confidenceInterval95pc'
                    stdMode=STD_CONFIDENCE_95PC;
                case 'none'
                    stdMode=STD_NONE;
            end
         end
    
    end
end