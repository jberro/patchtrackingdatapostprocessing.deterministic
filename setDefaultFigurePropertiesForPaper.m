% setDefaultFigurePropertiesForPaper:
% Sets the default figure properties to make nice plots with dimensions,
% fonts size, etc fit for publication
%
% Julien Berro
% 2012-2016
% Yale University

fontSize=10;%15;
titleSizeTimes=1.5;
markerSize=10;%9;
outerPosition=[0 0 8.9 8.9];

%figurePosition=[0.1 0.1 4.5 4.5]; % 1/2 column squared
%figurePosition=[0.1 0.1 6 6]; % 1/3 column squared
figurePosition=[0.1 0.1 8.9 8.9]; % 1 column squared
%figurePosition=[0.1 0.1 8.9 6.5]; % 1 column 2/3
%figurePosition=[0.1 0.1 18.2 18.2]; % 2 column squared

set(0,'DefaultFigureUnits','centimeters');
set(0,'DefaultAxesFontSize',fontSize);
set(0,'DefaultLineMarkerSize',markerSize);
set(0,'DefaultFigureColor',[1 1 1]);
set(0,'DefaultFigurePosition',figurePosition) ;
set(0,'DefaultLineLineWidth',1.5);
