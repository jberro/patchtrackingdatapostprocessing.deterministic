function allPatchData=loadAndPlotData_main(filePath)
% loadAndPlotData_main : 
% Main file for data alignment and plotting. Loads data described in filepath, a Matlab file with appropriate format (see sample file for more info), 
% containing information about data to use, figures to plot and alignment parameters to use
% Example: loadAndPlotData_main 'C:\Users\jb\Documents\MATLAB\PatchTrackingDataPostprocessing\experimentsFim1InPil1D_160219.m';
%
% Julien Berro
% 2012-2016
% Yale University


%% TODO
% 1. Box to choose datafile
% 2. Reload data option
% 3. nargin switch



dataDescriptionFilePath=filePath;
reloadData=true;
if reloadData||~exist('strainsToPlotCell')
    %% House keeping
    clearvars -except dataDescriptionFilePath
    LoadConstants
    useDataDescribedInMatlabFile=true;
    useDataDescribedInJSONFile=false; 
    plotAllDatapoints=false; %default: false
    indicesIndividualTracksToPlot={}; %default: {}
    colorsIndividualTracksToPlot={}; %default: {}
    divideActinConcentrationBy=1; %Actin Rescaling factor
    
       
    %% Parameters for continuous alignment (aka temporal super-resolution)
    nbPointsToForgetDuringAlignment=0;%default: 0 || Won't take into account the 'indexFirstForAlignment' first datapoint for alignement
    minNbDataPointsForAverages=5;%default: 3  || minimum number of datapoints at a time t to show the average and stdev (to avoid inacurate estimates)
    nbConsecutiveContinuousRealignements=5;%default: 3  || Number of consecutive realignments
    spillover=0;%default:0 to 2 || Number of allowed (measured?) datapoints that can "overhang" on each side during alignment (won't be taken into account for scoring)
    alignmentMode=ALIGNMENT_CONTINUOUS_ON_ALLDATA; txtAlignment=sprintf(' aligned on all data (continuously; %i realignements)',nbConsecutiveContinuousRealignements);
    scoreMode=SCORE_NOT_NORMALIZED; % Default: SCORE_NOT_NORMALIZED || Differences between raw signals are compared (favors peak values)|| Other possible value (experimental, Do not Use!): SCORE_NORMALIZED (Normalize (Log) the scores for each datapoint to debias peak values)
    reference_mode=REFERENCE_MEDIANROISET; % Default: REFERENCE_MEDIANROISET || Set which reference track is used for the first realignment. Possible values: REFERENCE_MEDIANROISET (a track with median length (less likely to get a false positive as reference); REFERENCE_LONGESTROISET (the longest track); REFERENCE_MOSTMEASUREDDATA (track with the largest number of measured values)

    scoreXStepSize=.5;%Default: .5 || Time step to evaluate the square difference between datasets
    precision=.1; %Default: .1 || Maximum temporal precision of the temporal super-resolution realignment 
    interpolationMethod='linear'; % Default: 'linear' || Interpolation method to infer data between two measured spots ; other possible value:'spline' (experimental, do not use)

    %% Default parameters for figures
    cleanFigures=true;
    fontSize=10;%15;
    titleSizeTimes=1;
    markerSize=5;%9;
    defaultLineWidth=1.5;
    figurePosition=[0.1 0.1 8.9 8.9]; % 1 column squared
    %set(0,'DefaultLineLineWidth',lineWidth);
    %figurePosition=[0.1 0.1 4.5 4.5]; % 1/2 column squared
    %figurePosition=[0.1 0.1 6 6]; % 1/3 column squared
    %outerPosition=[0 0 8.9 8.9];
    %figurePosition=[1 1 8.9 6.5]; % 1 column 2/3
    %figurePosition=[0.1 0.1 18.2 18.2]; % 2 column squared
    set(0,'DefaultFigureUnits','centimeters');
    set(0,'DefaultAxesFontSize',fontSize);
    set(0,'DefaultLineMarkerSize',markerSize);
    set(0,'DefaultFigureColor',[1 1 1]);
    set(0,'DefaultFigurePosition',figurePosition) ;
    set(0,'DefaultLineLineWidth',defaultLineWidth);
    set(0,'DefaultFigurePaperPositionMode','auto');
    set(0,'DefaultFigurePaperUnits','centimeters');
    set(0, 'DefaultFigurePaperSize', figurePosition(3:4)); %Set the paper to have width 5 and height 5.
    markerDensity=3;
    
    
    %% Special parameters (deprecated?)
    percentageMaxForTimingEstimation=.5; %
    intensityWindowSizeInPercentForTimingEstimation=0;
    normalizeData=false; % Experimental: True only for tests!! Default = false
    if (normalizeData)
        sprintf('WARNING! Intensities are normalized before alignment');
    end
    
    %% Parameters for traditional alignments (Deprecated)
    % alignmentMode=ALIGNMENT_ON_BEGINNING; txtAlignment=' aligned on beginning';
    % alignmentMode=ALIGNMENT_ON_END; txtAlignment=' aligned on end';
    % alignmentMode=ALIGNMENT_ON_ALLDATA; txtAlignment=' aligned on all data';
    % alignmentMode=ALIGNMENT_ON_PEAK; txtAlignment=' aligned on peak';
    % alignmentMode=ALIGNMENT_ON_SPEED; txtAlignment=' aligned on speed';
    % alignmentMode=ALIGNMENT_ON_PATHLENGTH; txtAlignment=' aligned on path length';
    minNbPointsOverlap=3; % for discrete alignments only %Note: =3 originally, but misalign fimbrin data
    
    %% Load the data described in the JSON file
    if useDataDescribedInMatlabFile
        [allPatchData,fuorecentProteinIDs,mutantIDs,offSetForFinalPlot,experimentDescriptionJsonFile] = loadPatchDataMatlabFile(dataDescriptionFilePath);
    elseif useDataDescribedInJSONFile
        [allPatchData,fuorecentProteinIDs,mutantIDs,offSetForFinalPlot,experimentDescriptionJsonFile] = loadPatchDataJSONFile(dataDescriptionFilePath);
    end
    markerDensity=experimentDescriptionJsonFile.reduceSymbolDensityBy;
    if ~isempty(experimentDescriptionJsonFile.pathToSaveFigures)
        pathFigureJSON=experimentDescriptionJsonFile.pathToSaveFigures;
        saveFigureJSON=true;
    else
        saveFigureJSON=false;
    end
    nbConsecutiveContinuousRealignements=experimentDescriptionJsonFile.dataStructure.otherParameters.nbConsecutiveContinuousRealignements;
    timeForContinuousAverage=experimentDescriptionJsonFile.dataStructure.otherParameters.timeForContinuousAverage;
    reduceSymbolDensityBy=experimentDescriptionJsonFile.dataStructure.otherParameters.reduceSymbolDensityBy;
    scoreXStepSize=experimentDescriptionJsonFile.dataStructure.otherParameters.scoreXStepSize;
    precision=experimentDescriptionJsonFile.dataStructure.otherParameters.precision;
    
    
    indicesNotEmpty=[];
    for i=1:size(fuorecentProteinIDs,2)
        for j=1:size(mutantIDs,2)
            expData=allPatchData{fuorecentProteinIDs(i)}{mutantIDs(j)};
            if ~isempty(expData)
                indicesNotEmpty=[indicesNotEmpty;[fuorecentProteinIDs(i) mutantIDs(j)]];
                expData.titleSizeTimes=titleSizeTimes;
            end
        end
    end
    
    fprintf('....Aligning all datasets with temporal super-resolution\n');
    nbIndicesNotEmpty=size(indicesNotEmpty,1);
    for i=1:nbIndicesNotEmpty
        curIndex=indicesNotEmpty(i,:);
        expData=allPatchData{curIndex(1,1)}{curIndex(1,2)};
        expData.cleanFigures=cleanFigures;
        
        expData.calibrate();
        findLongestRoisets(expData);
        
        if reference_mode==REFERENCE_LONGESTROISET  % reference track is the longest
            indexRef=expData.indexLongestRoiSet; % index of the roiset with the longest life time
        elseif reference_mode==REFERENCE_MOSTMEASUREDDATA  % reference track is the one with the largest number of measured values
            indexRef=expData.indexMostValueConcentration; % index of the roiset with the largest number of measured values
        elseif reference_mode==REFERENCE_MEDIANROISET % reference track is one with a median number of measured values
            indexRef=expData.indexMedianLengthRoiSet;
        end
        expData.computeBestAlignments(indexRef,nbPointsToForgetDuringAlignment,minNbPointsOverlap,scoreMode,scoreXStepSize,precision,interpolationMethod,spillover,normalizeData);
        expData.computeAllStatistics(percentageMaxForTimingEstimation,intensityWindowSizeInPercentForTimingEstimation);
        expData.computeAllContinuousStatistics(timeForContinuousAverage,interpolationMethod,percentageMaxForTimingEstimation,intensityWindowSizeInPercentForTimingEstimation);
        
        % Extra continuous realignment based on the successive continuous average
        for k=1:nbConsecutiveContinuousRealignements
            expData.computeBestAlignments(-1,nbPointsToForgetDuringAlignment,minNbPointsOverlap,scoreMode,scoreXStepSize,precision,interpolationMethod,spillover,normalizeData);
            expData.computeAllContinuousStatistics(timeForContinuousAverage,interpolationMethod,percentageMaxForTimingEstimation,intensityWindowSizeInPercentForTimingEstimation);
        end
    end
end


%% Initialisation of parameters and special plot options
offSetFigureNbArray=[1000 2000 3000 4000];
offSetCorrelationFigureNbArray=[1000 2000 3000 4000]; % Not used?
noErrorBarsForWT=false; % deprecated? Default: false

plotVsTime=true; % default: true (deprecated)
plotCorrelations=true; % default: true (deprecated)

% Non-standard plots (kept for legacy); includes special figures from Berro and Pollard MBoC 2014
plotVsTimeRatio=false; 
plotRelativeSD=false; % default: false; if true, plot coefficient of variation (SD/mean) instead of mean
plotDiffusionCoefficient=false;oneForRHZeroForD=3;plotVsNumberOfMolecule=0; % Plot diffusion coefficient, stokes radius, density etc; oneForRHZeroForD=3 for density
normalizeXInCorrelationPlots=false; % default: false; Normalizes X in correlation plots
normalizeYInCorrelationPlots=false; % default: false; Normalizes X in correlation plots
plotCytoplasmicConcentration=false; % default: false; Plots cytoplasmic concentrations ???
performStatisticalTests=false; % default: false; Performs statistical tests

addnumberTracksInLegend=true; % default: true; Adds number of tracks in the legend
showLegend=true; % default: true
showTitle=false; % default: false

saveDataFile=false; % Deprecated: Overriden by JSON file
pathDataFile=['C:\Users\jb246\Documents\Papiers\Mes Publis - Archives\Aip1\Revisions\Revised Aip1\New Figures\data\' datestr(now,'yymmddHHMMSS') '_'];% Deprecated: Overriden by JSON file

% Parameteres for alignment of different strains (e.g. to compare WT and mutant) using continuous alignment
alignOnFirstStrainPosition=false; % default: false || if true, aligns all strains on one feature ('field') of a reference strain ('indexStrainForAlignment') using the contimuous alignment method; if false, align strains such that their peak value is at time 0 (check?)
indexStrainForAlignment=1; % index of the strain used for alignment
%field='concentration';
field='speed'; %
spilloverFinalRealignment=5; % Number of allowed (measured?) datapoints that can "overhang" on each side during alignment (ie that won't be taken into account for scoring)(check?)
minNbPointsForAverageFinalRealignment=5; % Averaged values estimated with less than this number will not be taken into account in the scoring during realignment (check?) 

strainsToPlotCell={};
indicesIndividualTracksToPlot={};
dataToPlotIndices={};
figureTitleArray={};
correlationPlots={};
correlationFigureTitleArray={};
stdModes={};

%% Read the info about the plots described in the JSON or Matlab file
nb=0; % Counter for number of plots
if (useDataDescribedInMatlabFile || useDataDescribedInJSONFile)
    offSetFigureStyle=0;
    for indPlot=1:length(experimentDescriptionJsonFile.allPlots)
        nb=nb+1;
        textMutantCell{nb}='';
        [dataToPlotVsTime,titleFigureVsTime,correlationPlotsArray,titleFigureCorrelation,stdMode,...
            limits,ticks,strainsToPlot,curFiguresIndicesIndividualTracksToPlot,curFiguresColorsIndividualTracksToPlot]=...
            experimentDescriptionJsonFile.getAllPlotInfos(experimentDescriptionJsonFile.allPlots{indPlot});
        dataToPlotIndices{nb}=dataToPlotVsTime;
        figureTitleArray{nb}=titleFigureVsTime;
        correlationPlots{nb}=correlationPlotsArray;
        correlationFigureTitleArray{nb}=titleFigureCorrelation;
        %plotAllDatapoints=false;
        strainsToPlotCell{nb}=strainsToPlot;
        correlationPlotsCell{nb}=correlationPlots;
        indicesIndividualTracksToPlot{nb}=curFiguresIndicesIndividualTracksToPlot;
        colorsIndividualTracksToPlot{nb}=curFiguresColorsIndividualTracksToPlot;
        stdModes{nb}=stdMode;
        xLimitsCell{nb}=limits{1};
        xTickCell{nb}=ticks{1};
        for kk=2:7
            yLimitsCell{nb}{kk}=limits{kk};
            yTicksCell{nb}{kk}=ticks{kk};
        end
    end
end

%% Find best alignment between strains (e.g. to compare WT and mutant) (Check?)
if alignOnFirstStrainPosition
    for p=1:size(strainsToPlotCell,2)
        strainsToPlot=strainsToPlotCell{p};
        % Get reference strain info for alignment
        expDataRef=allPatchData{strainsToPlot(indexStrainForAlignment,1)}{strainsToPlot(indexStrainForAlignment,2)};
        meanRef=expDataRef.averageRoiSetContinuous.(field); % Average roiset, calculated on whole tracks alignemnt
        nbPointsRef=expDataRef.nbDataPointsRoiSetContinuous.(field); % Nb of data used at each time point to calculate the average on whole tracks alignemnt
        oriOffsetRef=expDataRef.getAlignmentOffset(alignmentMode);
        timeRef=(expDataRef.timeRoiSetContinuous-oriOffsetRef)*expDataRef.delayFrames;
        refArray=meanRef(nbPointsRef>minNbPointsForAverageFinalRealignment);
        xRefArray=timeRef(nbPointsRef>minNbPointsForAverageFinalRealignment);
        refArray0=refArray;
        xRefArray0=xRefArray;
        nbReruns=5;
        cm=colormap(jet(nbReruns+1));
        for j=1:(nbReruns+1);
            interpAlignedFunctionArray=zeros(length(xRefArray),size(strainsToPlot,1));
            for i=1:size(strainsToPlot,1)
                expData=allPatchData{strainsToPlot(i,1)}{strainsToPlot(i,2)};
                if (alignOnFirstStrainPosition&&i~=indexStrainForAlignment)
                    % Calculation of the best alignemt to the first strain
                    meanToAlign=expData.averageRoiSetContinuous.(field); % Average roiset, calculated on whole tracks alignemnt
                    nbPointsToAlign=expData.nbDataPointsRoiSetContinuous.(field); % Nb of data used at each time point to calculate the average on whole tracks alignemnt
                    oriOffsetToAlign=expData.getAlignmentOffset(alignmentMode);
                    timeToAlign=(expData.timeRoiSetContinuous-oriOffsetToAlign)*expData.delayFrames;
                    myArray=meanToAlign(nbPointsToAlign>minNbPointsForAverageFinalRealignment);
                    xMyArray=timeToAlign(nbPointsToAlign>minNbPointsForAverageFinalRealignment);
                    first0=1;
                    last0=length(refArray)-6;
                    scoreMode0=SCORE_NOT_NORMALIZED;
                    spillover0=spilloverFinalRealignment;
                    scoreXStepSize0=scoreXStepSize/5;
                    precision0=precision/5;
                    tools=ToolboxAlignment();
                    [bestContinuousScore,bestContinuousOffset,scoreFunction,functionRefArray,functionMyArray]=...
                        tools.getContinuousBestAlignment(refArray,xRefArray,myArray,xMyArray,first0,last0,scoreXStepSize0,precision0,interpolationMethod,spillover0,scoreMode0,normalizeData);
                    offSetForFinalPlot{strainsToPlot(i,1)}{strainsToPlot(i,2)}=bestContinuousOffset;
                    curInterpFunction=@(t) interp1(xMyArray,myArray,t-bestContinuousOffset,'linear',NaN);
                    interpAlignedFunctionArray(:,i)=curInterpFunction(xRefArray);
                else
                    curInterpFunction=@(t) interp1(xRefArray0,refArray0,t,'linear',NaN);
                    interpAlignedFunctionArray(:,i)=curInterpFunction(xRefArray);
                end
            end
            [mean,stdev,nbPoints]=computeStats(interpAlignedFunctionArray);
            refArray=mean;
            plot(xRefArray,mean,'Color',cm(j,:));
            plot(xRefArray,mean+stdev,'Color',cm(j,:));
        end
    end
end

maxConcentration={};
sdMaxConcentration={};
nMaxConcentration={};
textMaxConcentration={};


figureNb=100;
%% Make all plots versus time
if plotVsTime
    fprintf('....Plotting data versus time\n');
    for p=1:size(dataToPlotIndices,2)
        stdMode=stdModes{p};
        textErrorBars='';
        switch stdMode
            case STD_REGULAR||STD_MARKERS||STD_LINE||STD_LINE_AND_MARKERS
                textErrorBars=' +/- SD';
            case STD_STANDARD_ERROR
                textErrorBars=' +/- SEM';
            case STD_CONFIDENCE_95PC
                textErrorBars=' +/- CI_{95%}';
        end
        for indexFigure=1:size(dataToPlotIndices{p},2)
            figureNb=figureNb+1;
            dataToPlot=dataToPlotIndices{p}(indexFigure);
            figureTitle=strtrim(figureTitleArray{p}(indexFigure,:));
            textMutant=textMutantCell{p};
            xLimits=xLimitsCell{p};
            yLimits=yLimitsCell{p}{dataToPlot};
            strainsToPlot=strainsToPlotCell{p};
            figureNb=figureNb+1;
            f=figure(figureNb);clf;hold on;
            set(f,'Color',[1 1 1]);
            textLegend='';
            textLegendSD='';
            textLegendColor='';
            hPlots=[];
            allMarkers={};
            allMarkerSizes=[];
            hPlotsSD=[];
            % Variables to collect data to save in file
            % 1st line: strainName mutantName 'time' [time]
            % 2nd line: strainName mutantName 'mean' [mean]
            % 3rd line: strainName mutantName 'stdev' [stdev]
            dataToSave=cell(3*size(strainsToPlot,1),6);
            
            timesToSave=cell(size(strainsToPlot,1),1);
            meansToSave=cell(size(strainsToPlot,1),1);
            stdevsToSave=cell(size(strainsToPlot,1),1);
            strainNamesToSave=cell(size(strainsToPlot,1),2);
            
            for i=1:size(strainsToPlot,1)
                expData=allPatchData{strainsToPlot(i,1)}{strainsToPlot(i,2)};
                numberOfTracks=size(expData.roisets,2);
                bestContinuousOffset=offSetForFinalPlot{strainsToPlot(i,1)}{strainsToPlot(i,2)};
                % Plot
                if (useDataDescribedInMatlabFile || useDataDescribedInJSONFile)
                    [strain,mutant,color,lightColor,marker,lineWidth,textStyle,markerSize]=expData.plotOptions();
                else
                    [strain,mutant,color,lightColor,marker,lineWidth,textStyle,markerSize]=whichStrain(strainsToPlot(i,:),divideActinConcentrationBy);
                end
                allMarkers{i}=marker;
                allMarkerSizes=[allMarkerSizes markerSize];
                [maxNbRoiset,plotID,plotErrorbarID,offSetTime,time0,meanWithMinNbPoints,stdev]=...
                    expData.plotAverages(dataToPlot,alignmentMode,minNbDataPointsForAverages,stdMode,-bestContinuousOffset);
                
                maxConcentration{strainsToPlot(i,1)}{strainsToPlot(i,2)}=expData.maxValueContinuous.concentration;
                sdMaxConcentration{strainsToPlot(i,1)}{strainsToPlot(i,2)}=expData.sdMaxValueContinuous.concentration;
                nMaxConcentration{strainsToPlot(i,1)}{strainsToPlot(i,2)}=expData.nMaxValueContinuous.concentration;
                textMaxConcentration((i-1)*1+1,:)=[{strainsToPlot(i,1)} {strainsToPlot(i,2)} {strain} {mutant}...
                    {expData.maxValueContinuous.concentration} {expData.sdMaxValueContinuous.concentration} {expData.nMaxValueContinuous.concentration}];
                
                timesToSave(i,1)={time0};
                meansToSave(i,1)={meanWithMinNbPoints};
                stdevsToSave(i,1)={stdev};
                strainNamesToSave(i,1)={strain};
                strainNamesToSave(i,2)={mutant};
                
                dataToSave((i-1)*3+1,:)=[{strainsToPlot(i,1)} {strainsToPlot(i,2)} {strain} {mutant} {'time'} {time0}];
                dataToSave((i-1)*3+2,:)=[{strainsToPlot(i,1)} {strainsToPlot(i,2)} {strain} {mutant} {'mean'} {meanWithMinNbPoints}];
                dataToSave((i-1)*3+3,:)=[{strainsToPlot(i,1)} {strainsToPlot(i,2)} {strain} {mutant} {'stdev'} {stdev}];
                
                if plotAllDatapoints
                    plotAlsoAverage=false;
                    maxNbPoints=expData.plotPointPlotAndAverage(dataToPlot,alignmentMode,1,minNbDataPointsForAverages,indicesIndividualTracksToPlot,colorsIndividualTracksToPlot,plotAlsoAverage);
                end
                
                
                if (useDataDescribedInMatlabFile || useDataDescribedInJSONFile)
                    plotAlsoAverage=false;
                    maxNbPoints=expData.plotPointPlotAndAverage(dataToPlot,alignmentMode,1,minNbDataPointsForAverages,...
                        indicesIndividualTracksToPlot{p}{expData.fluorescentProtein}{expData.mutant},...
                        colorsIndividualTracksToPlot{p}{expData.fluorescentProtein}{expData.mutant},plotAlsoAverage);
                end
                
                %%Move error bars to the back
                uistack(plotErrorbarID,'bottom');
                eval(['set(plotID,' textStyle ');']);
                markerPlotId=reduceMarkerDensity(plotID,markerDensity);
                uistack(markerPlotId,'top');
                
                if stdMode==STD_MARKERS||stdMode==STD_LINE_AND_MARKERS
                    %set(plotErrorbarID,'Marker',marker);
                    eval(['set(plotErrorbarID,' textStyle ');']);
                    if  stdMode==STD_MARKERS&&strcmp(marker,'none')
                        %set(plotErrorbarID,'Marker',marker,'LineStyle','--');
                        set(plotErrorbarID,'LineStyle','--');
                    end
                end
                set(plotErrorbarID,'Color',lightColor);
                markerErrorId=reduceMarkerDensity(plotErrorbarID,markerDensity);
                uistack(markerErrorId,'bottom');
                
                if noErrorBarsForWT&&strainsToPlot(i,2)==MUTANT_WT
                    set(plotErrorbarID,'Visible','off');
                    set(markerErrorId,'Visible','off');
                end
                
                textLegend=char(textLegend,sprintf('%s in %s',strain,mutant));
                curTextLengendColor=['\color[rgb]{' sprintf('%f %f %f',color) '} ' sprintf('%s',strain)];
                if stdMode~=STD_NONE
                    curTextLengendColor=[curTextLengendColor '\color[rgb]{' sprintf('%f %f %f',lightColor) '}' textErrorBars];
                end
                curTextLengendColor=[curTextLengendColor '\color{black}' sprintf(' in %s',mutant)];
                if addnumberTracksInLegend
                    curTextLengendColor=[curTextLengendColor sprintf(' (%d)',numberOfTracks)];
                end
                textLegendColor=char(textLegendColor,curTextLengendColor);
                
                %textLegendSD{i}=sprintf('+/- SD %s in %s',strain,mutant);
                textLegendSD=char(textLegendSD,sprintf('%s %s in %s',textErrorBars,strain,mutant));
                hPlots=[hPlots plotID];
                %hPlotsSD{i}=plotErrorbarID(1);
                if max(size(plotErrorbarID)>0)
                    hPlotsSD=[hPlotsSD plotErrorbarID(1)];
                end
                %title([expData.name]);
            end
            textLegend=textLegend(2:size(textLegend,1),:);
            textLegendSD=textLegendSD(2:size(textLegendSD,1),:);
            textLegendColor=textLegendColor(2:size(textLegendColor,1),:);
            
            
            if size(xLimits)==[1 2]
                xlim(xLimits);
            end
            if size(yLimits)==[1 2]
                ylim(yLimits);
            end
             
            [legh,objh,outh,outm] =legend(hPlots,textLegendColor,'Location','Best');
            % Updates lines and markers (line properties are stored in
            % objh. Main markers are every 2 objects starting at 1/3
            % length +2. Next contains properties for minor marker
            for ind0=1:size(strainsToPlot,1)
                objInd=size(strainsToPlot,1)+2*(ind0);
                set(objh(objInd),'Marker',allMarkers{ind0},'MarkerSize',allMarkerSizes(ind0));
            end
            
            
            
            title([figureTitle textMutant],'FontSize',get(0,'DefaultAxesFontSize')*titleSizeTimes);
            
            if ~showLegend
                legend('off');
            end
            if ~showTitle
                title('remove');
            end
            
            %%% Save figure with legend if using JSON files
            if saveFigureJSON
                if ~exist(pathFigureJSON,'dir')
                    mkdir(pathFigureJSON);
                end
                fileName=sprintf('%s%sVsTime_%d_%d.pdf',pathFigureJSON,figureTitle,p,dataToPlot);
                %eval(['export_fig ''' fileName ''' -pdf -transparent']);
                print(fileName,'-dpdf');
            end
            
            
            if saveDataFile
                %pathDataFile='';
                txtDataToPlot=whichData(dataToPlot);
                fileNameDataTxt=sprintf('%sData_%d_%s.txt',pathDataFile,p,txtDataToPlot);
                fileNameDataMat=sprintf('%sData_%d_%s.mat',pathDataFile,p,txtDataToPlot);
                dlmcell(fileNameDataTxt,dataToSave);
                save(fileNameDataMat,'dataToSave');
            end
            
            cleanedFigureName=regexprep([figureTitle textMutant],'\\Delta','D');
            cleanedFigureName=regexprep(cleanedFigureName,'/','');
            set(f,'Name',cleanedFigureName);
            
            %end
        end
    end
end

%% Make plots of ratios versus time (Check?)
if plotVsTimeRatio
    fprintf('....Plotting ratios versus time\n');
    offSetFigureStyle=170000;
    for p=1:size(dataToPlotIndices,2)
        for indexFigure=1:size(dataToPlotIndices{p},2)
            dataToPlot=dataToPlotIndices{p}(indexFigure);
            figureTitle=['Ratio' strtrim(figureTitleArray{p}(indexFigure,:))];
            offSetFigureNb=offSetFigureNbArray(indexFigure);
            figureNb=figureNb+offSetFigureNb+offSetFigureStyle;
            % for p=1:size(strainsToPlotCell,2)
            textMutant=textMutantCell{p};
            xLimits=xLimitsCell{p};
            yLimits=yLimitsCell{p}{dataToPlot};
            strainsToPlot=strainsToPlotCell{p};
            figureNb=figureNb+1;
            f=figure(figureNb);clf;hold on;
            set(f,'Color',[1 1 1]);
            textLegend='';
            textLegendSD='';
            textLegendColor='';
            hPlots=[];
            allMarkers={};
            allMarkerSizes=[];
            %hPlotsSD=cell(1,size(strainsToPlot,1));
            hPlotsSD=[];
            % Variables to collect data to save in file
            % 1st line: strainName mutantName 'time' [time]
            % 2nd line: strainName mutantName 'mean' [mean]
            % 3rd line: strainName mutantName 'stdev' [stdev]
            dataToSave=cell(3*size(strainsToPlot,1),6);
            
            timesToSave=cell(size(strainsToPlot,1),1);
            meansToSave=cell(size(strainsToPlot,1),1);
            stdevsToSave=cell(size(strainsToPlot,1),1);
            strainNamesToSave=cell(size(strainsToPlot,1),2);
            
            strainRefRatio=allPatchData{strainsToPlot(1,1)}{strainsToPlot(1,2)};
            offsetstrainRefRatio=offSetForFinalPlot{strainsToPlot(1,1)}{strainsToPlot(1,2)};
            for i=1:size(strainsToPlot,1)
                expData=allPatchData{strainsToPlot(i,1)}{strainsToPlot(i,2)};
                numberOfTracks=size(expData.roisets,2);
                bestContinuousOffset=offSetForFinalPlot{strainsToPlot(i,1)}{strainsToPlot(i,2)};
                % Plot
                if (useDataDescribedInMatlabFile || useDataDescribedInJSONFile)
                    [strain,mutant,color,lightColor,marker,lineWidth,textStyle,markerSize]=expData.plotOptions();
                else
                    [strain,mutant,color,lightColor,marker,lineWidth,textStyle,markerSize]=whichStrain(strainsToPlot(i,:),divideActinConcentrationBy);
                end
                allMarkers{i}=marker;
                allMarkerSizes=[allMarkerSizes markerSize];
                [maxNbRoiset,plotID,plotErrorbarID,offSetTime,time0,meanWithMinNbPoints,stdev]=...
                    expData.plotAverages(dataToPlot,alignmentMode,minNbDataPointsForAverages,STD_NONE,-bestContinuousOffset,strainRefRatio,offsetstrainRefRatio);
                
                timesToSave(i,1)={time0};
                meansToSave(i,1)={meanWithMinNbPoints};
                stdevsToSave(i,1)={stdev};
                strainNamesToSave(i,1)={strain};
                strainNamesToSave(i,2)={mutant};
                
                dataToSave((i-1)*3+1,:)=[{strainsToPlot(i,1)} {strainsToPlot(i,2)} {strain} {mutant} {'time'} {time0}];
                dataToSave((i-1)*3+2,:)=[{strainsToPlot(i,1)} {strainsToPlot(i,2)} {strain} {mutant} {'mean'} {meanWithMinNbPoints}];
                dataToSave((i-1)*3+3,:)=[{strainsToPlot(i,1)} {strainsToPlot(i,2)} {strain} {mutant} {'stdev'} {stdev}];
                
                %%Move error bars to the back
                uistack(plotErrorbarID,'bottom');
                
                eval(['set(plotID,' textStyle ');']);
                
                markerPlotId=reduceMarkerDensity(plotID,markerDensity);
                uistack(markerPlotId,'top');
                
                if stdMode==STD_MARKERS||stdMode==STD_LINE_AND_MARKERS
                    %set(plotErrorbarID,'Marker',marker);
                    eval(['set(plotErrorbarID,' textStyle ');']);
                    if  stdMode==STD_MARKERS&&strcmp(marker,'none')
                        %set(plotErrorbarID,'Marker',marker,'LineStyle','--');
                        set(plotErrorbarID,'LineStyle','--');
                    end
                end
                set(plotErrorbarID,'Color',lightColor);
                markerErrorId=reduceMarkerDensity(plotErrorbarID,markerDensity);
                uistack(markerErrorId,'bottom');
                
                if noErrorBarsForWT&&strainsToPlot(i,2)==MUTANT_WT
                    set(plotErrorbarID,'Visible','off');
                    set(markerErrorId,'Visible','off');
                end
                
                textLegend=char(textLegend,sprintf('%s in %s',strain,mutant));
                curTextLengendColor=['\color[rgb]{' sprintf('%f %f %f',color) '} ' sprintf('%s',strain)];
                if stdMode~=STD_NONE
                    curTextLengendColor=[curTextLengendColor '\color[rgb]{' sprintf('%f %f %f',lightColor) '}' textErrorBars];
                end
                curTextLengendColor=[curTextLengendColor '\color{black}' sprintf(' in %s',mutant)];
                if addnumberTracksInLegend
                    curTextLengendColor=[curTextLengendColor sprintf(' (%d)',numberOfTracks)];
                end
                textLegendColor=char(textLegendColor,curTextLengendColor);
                
                textLegendSD=char(textLegendSD,sprintf('%s %s in %s',textErrorBars,strain,mutant));
                hPlots=[hPlots plotID];
                if max(size(plotErrorbarID)>0)
                    hPlotsSD=[hPlotsSD plotErrorbarID(1)];
                end
            end
            textLegend=textLegend(2:size(textLegend,1),:);
            textLegendSD=textLegendSD(2:size(textLegendSD,1),:);
            textLegendColor=textLegendColor(2:size(textLegendColor,1),:);
            
            
            if size(xLimits)==[1 2]
                xlim(xLimits);
            end
            title([figureTitle textMutant],'FontSize',get(0,'DefaultAxesFontSize')*titleSizeTimes);
        end
        
        if saveDataFile
            txtDataToPlot=whichData(dataToPlot);
            fileNameDataTxt=sprintf('%sData_%d_%s_RatioTo.txt',pathDataFile,p,txtDataToPlot);
            fileNameDataMat=sprintf('%sData_%d_%s_RatioTo.mat',pathDataFile,p,txtDataToPlot);
            dlmcell(fileNameDataTxt,dataToSave);
            save(fileNameDataMat,'dataToSave');
        end
        
        cleanedFigureName=regexprep([figureTitle textMutant],'\\Delta','D');
        cleanedFigureName=regexprep(cleanedFigureName,'/','');
        set(f,'Name',cleanedFigureName);
        if ~showLegend
            legend('off');
        end
        if ~showTitle
            title('remove');
        end
    end
end



%% Plot relative standard deviation
if plotRelativeSD
    fprintf('....Plotting relative standard deviation\n');
    for p=1:size(dataToPlotIndices,2)
        for indexFigure=1:size(dataToPlotIndices{p},2)
            dataToPlot=dataToPlotIndices{p}(indexFigure);
            figureTitle=strtrim(figureTitleArray{p}(indexFigure,:));
            offSetFigureNb=offSetFigureNbArray(indexFigure);
            figureNb=figureNb+offSetFigureNb+offSetFigureStyle+10000;
            textMutant=textMutantCell{p};
            strainsToPlot=strainsToPlotCell{p};
            figureNb=figureNb+1;
            f=figure(figureNb);clf;hold on;
            set(f,'Color',[1 1 1]);
            textLegend='';
            textLegendSD='';
            textLegendColor='';
            hPlots=[];
            allMarkers={};
            allMarkerSizes=[];
            hPlotsSD=[];
            for i=1:size(strainsToPlot,1)
                expData=allPatchData{strainsToPlot(i,1)}{strainsToPlot(i,2)};
                numberOfTracks=size(expData.roisets,2);
                bestContinuousOffset=offSetForFinalPlot{strainsToPlot(i,1)}{strainsToPlot(i,2)};
                % Plot
                if (useDataDescribedInMatlabFile || useDataDescribedInJSONFile)
                    [strain,mutant,color,lightColor,marker,lineWidth,textStyle,markerSize]=expData.plotOptions();
                else
                    [strain,mutant,color,lightColor,marker,lineWidth,textStyle,markerSize]=whichStrain(strainsToPlot(i,:),divideActinConcentrationBy);
                end
                allMarkers{i}=marker;
                allMarkerSizes=[allMarkerSizes markerSize];
                [maxNbPoints,plotID,offSetTime]=expData.plotRelativeSD(dataToPlot,alignmentMode,minNbDataPointsForAverages,-bestContinuousOffset);
                
                eval(['set(plotID,' textStyle ');']);
                
                markerPlotId=reduceMarkerDensity(plotID,markerDensity);
                uistack(markerPlotId,'top');
                
                textLegend=char(textLegend,sprintf('%s in %s',strain,mutant));
                curTextLengendColor=['\color[rgb]{' sprintf('%f %f %f',color) '} ' sprintf('%s',strain)];
                
                curTextLengendColor=[curTextLengendColor '\color{black}' sprintf(' in %s',mutant)];
                if addnumberTracksInLegend
                    curTextLengendColor=[curTextLengendColor sprintf(' (%d)',numberOfTracks)];
                end
                textLegendColor=char(textLegendColor,curTextLengendColor);
                
                textLegendSD=char(textLegendSD,sprintf('%s in %s',strain,mutant));
                hPlots=[hPlots plotID];
            end
            textLegend=textLegend(2:size(textLegend,1),:);
            textLegendSD=textLegendSD(2:size(textLegendSD,1),:);
            textLegendColor=textLegendColor(2:size(textLegendColor,1),:);
            
            [legh,objh,outh,outm] =legend(hPlots,textLegendColor,'Location','Best');
            % Updates lines and markers (line properties are stored in
            % objh. Main markers are every 2 objects starting at 1/3
            % length +2. Next contains properties for minor marker
            for ind0=1:size(strainsToPlot,1)
                objInd=size(strainsToPlot,1)+2*(ind0);
                set(objh(objInd),'Marker',allMarkers{ind0},'MarkerSize',allMarkerSizes(ind0));
            end
            
            title([figureTitle textMutant],'FontSize',get(0,'DefaultAxesFontSize')*titleSizeTimes);
            
            cleanedFigureName=regexprep([figureTitle textMutant],'\\Delta','D');
            cleanedFigureName=regexprep(cleanedFigureName,'/','');
            set(f,'Name',cleanedFigureName);
            if ~showLegend
                legend('off');
            end
            if ~showTitle
                title('remove');
            end
        end
    end
end


%% Make all correlation plots
if plotCorrelations
    fprintf('....Plotting correlations\n');
    % By default, do not plot any error to avoid clutering the figure
    %stdMode=STD_LINE;
    %stdMode=STD_LINE_AND_MARKERS;
    %stdMode=STD_MARKERS;
    stdMode=STD_NONE;
    %stdMode=STD_REGULAR;
    %stdMode=STD_STANDARD_ERROR;
    offSetFigureStyle=1000;
    for p=1:size(dataToPlotIndices,2)
        for indexFigure=1:size(correlationPlots{p},1)
            xDataToPlotID=correlationPlots{p}(indexFigure,1);
            yDataToPlotID=correlationPlots{p}(indexFigure,2);
            figureTitle=strtrim(correlationFigureTitleArray{p}(indexFigure,:));
            offSetCorrelationFigureNb=offSetFigureNbArray(indexFigure);
            figureNb=offSetCorrelationFigureNb+offSetFigureStyle;
            textMutant=textMutantCell{p};
            xLimits=yLimitsCell{p}{xDataToPlotID};
            yLimits=yLimitsCell{p}{yDataToPlotID};
            strainsToPlot=strainsToPlotCell{p};
            figureNb=figureNb+1;
            f=figure(figureNb);clf;hold on;
            set(f,'Color',[1 1 1]);
            textLegend='';
            allMarkers={};
            allMarkerSizes=[];
            hPlots=[];
            for i=1:size(strainsToPlot,1)
                expData=allPatchData{strainsToPlot(i,1)}{strainsToPlot(i,2)};
                bestContinuousOffset=offSetForFinalPlot{strainsToPlot(i,1)}{strainsToPlot(i,2)};
                % Plot
                if (useDataDescribedInMatlabFile || useDataDescribedInJSONFile)
                    [strain,mutant,color,lightColor,marker,lineWidth,textStyle,markerSize]=expData.plotOptions();
                else
                    [strain,mutant,color,lightColor,marker,lineWidth,textStyle,markerSize]=whichStrain(strainsToPlot(i,:),divideActinConcentrationBy);
                end
                markerEveryN=1; % plot all symbols
                %markerEveryN=5; % plot symbol every 5 points
                [plotID,plotVertErrorbarID,plotHorizErrorbarID]=...
                    expData.plotCorrelationAverage(xDataToPlotID,yDataToPlotID,alignmentMode,minNbDataPointsForAverages,stdMode,normalizeXInCorrelationPlots,normalizeYInCorrelationPlots,markerEveryN);
                set(plotVertErrorbarID,'Color',lightColor);
                set(plotHorizErrorbarID,'Color',lightColor);
                eval(['set(plotID,' textStyle ');']);
                markerPlotId=reduceMarkerDensity(plotID,markerDensity);
                uistack(markerPlotId,'top');
                
                %%Move error bars to the back
                uistack(plotHorizErrorbarID,'bottom');
                uistack(plotVertErrorbarID,'bottom');
                
                if plotAllDatapoints
                    expData.plotCorrelation(xDataToPlotID,yDataToPlotID,1,indicesIndividualTracksToPlot,colorsIndividualTracksToPlot);
                end
                
                textLegend=char(textLegend,sprintf('%s in %s',strain,mutant));
                allMarkers{i}=marker;
                allMarkerSizes=[allMarkerSizes markerSize];
                hPlots=[hPlots plotID];
            end
            textLegend=textLegend(2:size(textLegend,1),:);
            % Update legend lines and markers
            if size(xLimits)==[1 2]
                xlim(xLimits);
            end
            if size(yLimits)==[1 2]
                ylim(yLimits);
            end
            
            if normalizeXInCorrelationPlots
                xlim('auto');
            end
            if normalizeYInCorrelationPlots
                ylim('auto');
            end
            
            [legh,objh,outh,outm] =legend(hPlots,textLegend,'Location','Best');
            % Updates lines and markers (line properties are stored in
            % objh. Main markers are every 2 objects starting at 1/3
            % length +2. Next contains properties for minor marker
            for ind0=1:size(strainsToPlot,1)
                objInd=size(strainsToPlot,1)+2*(ind0);
                set(objh(objInd),'Marker',allMarkers{ind0},'MarkerSize',allMarkerSizes(ind0));
            end
            title([figureTitle textMutant],'FontSize',get(0,'DefaultAxesFontSize')*titleSizeTimes);
            
            if ~showLegend
                legend('off');
            end
            if ~showTitle
                title('remove');
            end
            
            %%% Save figure if using JSON files
            if saveFigureJSON
                if ~exist(pathFigureJSON,'dir')
                    mkdir(pathFigureJSON);
                end
                fileName=sprintf('%sCorrelationPlot_%s_%d_%d.pdf',pathFigureJSON,figureTitle,p,dataToPlot);
                print(fileName,'-dpdf');
            end
            
            cleanedFigureName=regexprep([figureTitle textMutant],'\\Delta','D');
            cleanedFigureName=regexprep(cleanedFigureName,'/','');
            set(f,'Name',cleanedFigureName);
        end
    end
end

%% Deprecated, kept for legacy (figure from Berro and Pollard 2014)
% DO NOT DELETE (TODO: transform into more generic figure plotting)
if plotCytoplasmicConcentration
    fprintf('....Plotting cytoplasmic concentrations\n');
    LoadCytoplasmData;
    %stdMode=STD_LINE;
    %stdMode=STD_LINE_AND_MARKERS;
    %stdMode=STD_MARKERS;
    stdMode=STD_NONE;
    %stdMode=STD_REGULAR;
    offSetFigureNb=5000;
    offSetFigureStyle=700;
    
    textCytoplasmCell{1}=' - mEGFP-actin';
    cytoplasmicPlots{1}=[...
        STRAINS_GFP_ACTIN MUTANT_WT;...
        STRAINS_GFP_ACTIN MUTANT_ACP1D;...
        STRAINS_GFP_ACTIN MUTANT_ACP2D;...
        STRAINS_GFP_ACTIN MUTANT_AIP1D;...
        STRAINS_GFP_ACTIN MUTANT_ACP1DACP2D;...
        STRAINS_GFP_ACTIN MUTANT_ACP1DAIP1D...
        ];
    figureTitle='mEGFP-actin Cytoplasmic Concentration (\muM)';
    
    % dataToPlot=dataToPlotIndices(indexFigure); % concentration ....
    %    figureTitle=strtrim(figureTitleArray(indexFigure,:));
    figureNb=offSetFigureNb+offSetFigureStyle;
    for p=1:size(cytoplasmicPlots,2)
        %textMutant=textCytoplasm{p};
        %xLimits=xLimitsCell{p};
        %yLimits=yLimitsCell{p}{dataToPlot};
        cytoplasmsToPlot=cytoplasmicPlots{p};
        figureNb=figureNb+1;
        f=figure(figureNb);clf;hold on;
        set(f,'Color',[1 1 1]);
        textLegend='';
        hPlots=[];
        avs=[];
        stds=[];
        for i=1:size(cytoplasmsToPlot,1)
            curAvData=cytoplasmicConcentrationsAverage{cytoplasmsToPlot(i,1)}{cytoplasmsToPlot(i,2)};
            curStdData=cytoplasmicConcentrationsStd{cytoplasmsToPlot(i,1)}{cytoplasmsToPlot(i,2)};
            if (useDataDescribedInMatlabFile || useDataDescribedInJSONFile)
                [strain,mutant,color,lightColor,marker,lineWidth,textStyle,markerSize]=curAvData.plotOptions();
            else
                [strain,mutant,color,lightColor,marker,lineWidth,textStyle]=whichStrain(cytoplasmsToPlot(i,:),divideActinConcentrationBy);
            end
            xLabels{i}=mutant;
            avs=[avs curAvData];
            stds=[stds curStdData];
            bar(i,curAvData,'EdgeColor',color,'FaceColor',[1 1 1]);
            errorbar(i,curAvData,curStdData,'Color',lightColor);
            h=plot(i,curAvData);%,'Color',color,'Marker',marker,'MarkerSize',10);
            eval(['set(h,' textStyle ');']);
            
        end
        title(figureTitle);
        fontSizeXLabels=fontSize*titleSizeTimes;
        ylabel([strain ' (\muM)']);%,'FontSize',fontSizeXLabels);
        ylim([0 max(avs+1.2*stds)]);
        set(gca,'XTick',1:size(cytoplasmsToPlot,1),'XTickLabel',xLabels,'FontSize',fontSize);
        %% Rotate the labels
        rot=30;
        a=get(gca,'XTickLabel');
        %erase current tick labels from figure
        set(gca,'XTickLabel',[]);
        %get tick label positions
        b=get(gca,'XTick');
        c=get(gca,'YTick');
        %make new tick labels
        if rot<180
            orientation='right';
        else
            orientation='leftt';
        end
        
        axisPos=get(gca,'Position');
        percentage=0.1;
        newPos=[axisPos(1) axisPos(2)+percentage*axisPos(4) axisPos(3) (1-percentage)*axisPos(4)];
        set(gca,'Position',newPos);
        
        %t=text(b+.3*(b(2)-b(1)),repmat(c(1)-.1*(c(2)-c(1)),length(b),1),a,'HorizontalAlignment',orientation,'VerticalAlignment','top','rotation',rot,'FontSize',fontSize);
        t=text(b,repmat(c(1)-.1*(c(2)-c(1)),length(b),1),a,'HorizontalAlignment',orientation,'VerticalAlignment','top','rotation',rot,'FontSize',fontSize);
        
        %         for i = 1:length(t)
        %             ext(i,:) = get(t(i),'Extent');
        %         end
        %
        %         % Determine the lowest point. The X-label will be
        %         % placed so that the top is aligned with this point.
        %         LowYPoint = min(ext(:,2));
        %
        %         % Place the axis label at this point
        %         XMidPoint = 1+1/2;
        %         tl = text(XMidPoint,LowYPoint,'X-Axis Label', ...
        %             'VerticalAlignment','bottom', ...
        %             'HorizontalAlignment','center','FontSize',fontSize);
    end
end

%% Plots diffusion coefficient or Stokes radius (Deprecated, used for figures in Berro and Pollard 2014)
if plotDiffusionCoefficient
    fprintf('....Plotting diffusion coefficients, Stokes radii, etc\n');
    offSetFigureNb=10000;
    figureNb=offSetFigureNb+1;
    f=figure(figureNb);clf;hold on;
    for p=1:size(strainsToPlotCell,2)
        textMutant=textMutantCell{p};
        xLimits=xLimitsCell{p};
        yLimits='auto';
        strainsToPlot=strainsToPlotCell{p};
        set(f,'Color',[1 1 1]);
        textLegend='';
        textLegendSD='';
        textLegendColor='';
        hPlots=[];
        allMarkers={};
        allMarkerSizes=[];
        hPlotsSD=[];
        % Variables to collect data to save in file
        % 1st line: strainName mutantName 'time' [time]
        % 2nd line: strainName mutantName 'mean' [mean]
        % 3rd line: strainName mutantName 'stdev' [stdev]
        dataToSave=cell(3*size(strainsToPlot,1),6);
        
        timesToSave=cell(size(strainsToPlot,1),1);
        meansToSave=cell(size(strainsToPlot,1),1);
        stdevsToSave=cell(size(strainsToPlot,1),1);
        strainNamesToSave=cell(size(strainsToPlot,1),2);
        
        for i=1:size(strainsToPlot,1)
            expData=allPatchData{strainsToPlot(i,1)}{strainsToPlot(i,2)};
            numberOfTracks=size(expData.roisets,2);
            bestContinuousOffset=offSetForFinalPlot{strainsToPlot(i,1)}{strainsToPlot(i,2)};
            % Plot
            if (useDataDescribedInMatlabFile || useDataDescribedInJSONFile)
                [strain,mutant,color,lightColor,marker,lineWidth,textStyle,markerSize]=expData.plotOptions();
            else
                [strain,mutant,color,lightColor,marker,lineWidth,textStyle,markerSize]=whichStrain(strainsToPlot(i,:),divideActinConcentrationBy);
            end
            allMarkers{i}=marker;
            allMarkerSizes=[allMarkerSizes markerSize];
            [maxNbRoiset,plotID,offSetTime,time0,meanWithMinNbPoints,stdev,plotID_SEM]=...
                expData.plotDiffusionCoefficient(plotVsNumberOfMolecule,oneForRHZeroForD,alignmentMode,minNbDataPointsForAverages,-bestContinuousOffset);
            timesToSave(i,1)={time0};
            meansToSave(i,1)={meanWithMinNbPoints};
            stdevsToSave(i,1)={stdev};
            strainNamesToSave(i,1)={strain};
            strainNamesToSave(i,2)={mutant};
            
            dataToSave((i-1)*3+1,:)=[{strainsToPlot(i,1)} {strainsToPlot(i,2)} {strain} {mutant} {'time'} {time0}];
            dataToSave((i-1)*3+2,:)=[{strainsToPlot(i,1)} {strainsToPlot(i,2)} {strain} {mutant} {'mean'} {meanWithMinNbPoints}];
            dataToSave((i-1)*3+3,:)=[{strainsToPlot(i,1)} {strainsToPlot(i,2)} {strain} {mutant} {'stdev'} {stdev}];
            
            
            eval(['set(plotID,' textStyle ');']);
            
            set(plotID_SEM,'Color',lightColor);
            markerErrorId=reduceMarkerDensity(plotID_SEM,markerDensity);
            uistack(markerErrorId,'bottom');
            
            
            markerPlotId=reduceMarkerDensity(plotID,markerDensity);
            uistack(markerPlotId,'top');
            
            textLegend=char(textLegend,sprintf('%s in %s',strain,mutant));
            curTextLengendColor=['\color[rgb]{' sprintf('%f %f %f',color) '} ' sprintf('%s',strain)];
            if stdMode~=STD_NONE
                curTextLengendColor=[curTextLengendColor '\color[rgb]{' sprintf('%f %f %f',lightColor) '}' textErrorBars];
            end
            curTextLengendColor=[curTextLengendColor '\color{black}' sprintf(' in %s',mutant)];
            if addnumberTracksInLegend
                curTextLengendColor=[curTextLengendColor sprintf(' (%d)',numberOfTracks)];
            end
            textLegendColor=char(textLegendColor,curTextLengendColor);
            
            textLegendSD=char(textLegendSD,sprintf('%s %s in %s',textErrorBars,strain,mutant));
            hPlots=[hPlots plotID];
        end
        
        textLegend=textLegend(2:size(textLegend,1),:);
        textLegendSD=textLegendSD(2:size(textLegendSD,1),:);
        textLegendColor=textLegendColor(2:size(textLegendColor,1),:);
        
        
        if size(xLimits)==[1 2]
            xlim(xLimits);
        end
        if size(yLimits)==[1 2]
            ylim(yLimits);
        end
        
        % Limit X to positive values only
        xlimits=xlim;xlim([0 xlimits(2)]);
        % Force Y to start at 0
        ylimits=ylim;ylim([0 ylimits(2)]);
        
        %%Legend
        [legh,objh,outh,outm] =legend(hPlots,textLegendColor,'Location','Best');
        % Updates lines and markers (line properties are stored in
        % objh. Main markers are every 2 objects starting at 1/3
        % length +2. Next contains properties for minor marker
        for ind0=1:size(strainsToPlot,1)
            objInd=size(strainsToPlot,1)+2*(ind0);
            set(objh(objInd),'Marker',allMarkers{ind0},'MarkerSize',allMarkerSizes(ind0));
        end
        
        if saveDataFile
            fileNameDataTxt=sprintf('%sData_%d_DiffusionCoefficient.txt',pathDataFile,p);
            fileNameDataMat=sprintf('%sData_%d_DiffusionCoefficient.mat',pathDataFile,p);
            dlmcell(fileNameDataTxt,dataToSave);
            save(fileNameDataMat,'dataToSave');
        end
        
        if ~showLegend
            legend('off');
        end
        if ~showTitle
            title('remove');
        end
    end
    
end


%% Statistical tests on nb molecules on the last data set (Deprecated, used for figures in Berro and Pollard 2014)
if performStatisticalTests
    fprintf('....Performing statistical tests\n');
    nbStrains=length(strainsToPlot);
    tStud=zeros(nbStrains);
    nuStud=zeros(nbStrains);
    tThreshold=zeros(nbStrains);
    tThreshold2=zeros(nbStrains);
    pValues=zeros(nbStrains);
    pValues0=zeros(nbStrains);
    for i=1:nbStrains
        curProteinID1=strainsToPlot(i,1);
        curMutantID1=strainsToPlot(i,2);
        expData1=allPatchData{curProteinID1}{curMutantID1};
        m1=expData1.maxValueContinuous.concentration;
        v1=expData1.sdMaxValueContinuous.concentration^2;
        N1=expData1.nMaxValueContinuous.concentration;
        nu1=N1-1;
        for j=i+1:nbStrains
            curProteinID2=strainsToPlot(j,1);
            curMutantID2=strainsToPlot(j,2);
            expData2=allPatchData{curProteinID2}{curMutantID2};
            m2=expData2.maxValueContinuous.concentration;
            v2=expData2.sdMaxValueContinuous.concentration^2;
            N2=expData2.nMaxValueContinuous.concentration;
            nu2=N2-1;
            nuStud(i,j)=(v1/N1+v2/N2)^2/(v1^2/(N1^2*nu1)+v2^2/(N2^2*nu2));
            tStud(i,j)=(m1-m2)/sqrt(v1/N1+v2/N2);
            alpha=0.05;
            tThreshold(i,j)=tinv(1-alpha/2,nuStud(i,j));
            pValues(i,j) = 2*(1-tcdf(abs(tStud(i,j)),nuStud(i,j)));
            pValues0(i,j) = 2*(1-tcdf(tThreshold(i,j),nuStud(i,j))); % control
            w1=v1/N1;
            w2=v2/N2;
            tThreshold2(i,j)=(tinv(1-alpha/2,nu1)*w1+tinv(1-alpha/2,nu2)*w2)/(w1+w2);
        end
    end
    pValues
end

fprintf('....All done successfully!\n');
    
