function [mean,stdev,nbPoints]=computeStats(data,weights)
%% Computes statistics of a n*m array for each line
% if the argument weights is passed, the function returns the weighted
% averages and stdev
%
% Julien Berro
% 2012-2016
% Yale University

if (nargin == 1)
    weights = ones(size(data));
end

[nbRow nbCol]=size(data);
mean=NaN*ones(nbRow,1);
stdev=NaN*ones(nbRow,1);
nbPoints=NaN*ones(nbRow,1);
for i=1:nbRow
    tmpMean=0;
    tmpStdev=0;
%     tmpNbPoints=0;
      tmpTotalWeight=0;
    for j=1:nbCol
        curValue=data(i,j);
        if ~isnan(curValue)
        %if ~isnan(curValue)&&curValue>0
%             tmpMean=tmpMean+curValue;
%             tmpNbPoints=tmpNbPoints+1;
            curWeight=weights(i,j);
            tmpMean=tmpMean+curWeight*curValue;
            tmpTotalWeight=tmpTotalWeight+curWeight;
        end
    end
%     tmpMean=tmpMean/tmpNbPoints;
    tmpMean=tmpMean/tmpTotalWeight;

    for j=1:nbCol
        curValue=data(i,j);
        if ~isnan(curValue)
        %if ~isnan(curValue)&&curValue>0
%             tmpStdev=tmpStdev+(curValue-tmpMean)^2;
            curWeight=weights(i,j);
            %tmpTotalWeight=tmpTotalWeight+curWeight;
            tmpStdev=tmpStdev+curWeight*(curValue-tmpMean)^2;
        end
    end
%    tmpStdev=tmpStdev/(tmpNbPoints-1);
    tmpStdev=tmpStdev/tmpTotalWeight;
    mean(i,1)=tmpMean;
    stdev(i,1)=sqrt(tmpStdev);
%     nbPoints(i,1)=tmpNbPoints;
    nbPoints(i,1)=tmpTotalWeight;
end
end
